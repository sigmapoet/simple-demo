# demo一览

## 应用demo

### HikViSionPicReadDemo
- python语言编写，可以完成对海康威视摄像头rtsp流的捕捉和播放，即拉流

### YUVNV12ToRGBandBMPDemo
- C/C++语言编写，可以完成YUVNV12颜色空间图片向RGB颜色空间的转换，也可以将RGB图像转换成BMP图片

### ellispeDetectionDemo

- C/C++语言编写，依赖opencv2.4.10，可以完成对图片中椭圆的检测，最准确的一版请翻找提交记录，看最开始的一版。这个demo也有在aarch64上运行的版本，请翻找提交记录

### realtimeCannyDemo

- 基于荣品hi3516dv300 demo板，实现了实时canny边缘提取，对摄像头拍摄的图像，实时canny，然后将结果输出到显示器。帧率多少忘记了。

### tcpServerForUpdateDemo

- 基于tcp协议，为解码机进行固件升级的demo。



## 开发demo

### myMakefileDemo

- make和C/C++，可以完成C/C++源代码的自动依赖，以make方式构建工程可以使用以makefile demo

### driverOutsideKernelTree

- 一个在内核树外构建模块的例子，主要参考的是Makefile的写法



## 测试demo

### opencvEdgeDetectionDemo

- C++语言编写，对opencv中各边缘检测算法进行了使用和测试，可作为使用样例，也可以进行效率对比



