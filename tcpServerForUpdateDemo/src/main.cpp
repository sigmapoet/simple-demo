#define LOG_TAG    "tcpServer"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>  
#include <unistd.h>  
#include <stdlib.h>
#include <sys/time.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string>

//#define USE_CRYPTOPP          //选择使用cryptopp开源库中的md5还是自己的md5算法

#include "elog.h"
#ifdef USE_CRYPTOPP         
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include "cryptlib.h"
#include "md5.h"
#include "files.h"
#include "hex.h"
#endif

#define MD5LEN              (64)
#define CMDLEN              (200)
#define TCPPORT             (2023)
#define BUFLEN              (2048)
#define FIRMWARE_NAMELEN    (20)
#define FIRMWARE_PATHLEN    (200)
#define FIRMWARE_FILEMAXLEN (2000000)
#define FIRMWARE_FILE       "hytq-vdec-5d"
#define APPPATH             "/root/application"
#define LOGPATH             "/root/application/elog_tmpfs"

#ifdef USE_CRYPTOPP
using namespace CryptoPP;
#endif

#define LOG_TAG     "eLogInit"
void eLogInit()
{
    /* close printf buffer */
    setbuf(stdout, NULL);
    /* initialize EasyLogger */
    elog_init();
    /* set EasyLogger log format */
    elog_set_fmt(ELOG_LVL_ASSERT, ELOG_FMT_ALL);
    elog_set_fmt(ELOG_LVL_ERROR, ELOG_FMT_LVL | ELOG_FMT_TAG | ELOG_FMT_TIME);
    elog_set_fmt(ELOG_LVL_WARN, ELOG_FMT_LVL | ELOG_FMT_TAG | ELOG_FMT_TIME);
    elog_set_fmt(ELOG_LVL_INFO, ELOG_FMT_LVL | ELOG_FMT_TAG | ELOG_FMT_TIME);
    elog_set_fmt(ELOG_LVL_DEBUG, ELOG_FMT_ALL & ~ELOG_FMT_FUNC);
    elog_set_fmt(ELOG_LVL_VERBOSE, ELOG_FMT_ALL & ~ELOG_FMT_FUNC);
#ifdef ELOG_COLOR_ENABLE
    elog_set_text_color_enabled(true);
#endif
    /* start EasyLogger */
    elog_start();
    /* dynamic set enable or disable for output logs (true or false) */
//    elog_set_output_enabled(false);
    /* dynamic set output logs's level (from ELOG_LVL_ASSERT to ELOG_LVL_VERBOSE) */
//    elog_set_filter_lvl(ELOG_LVL_WARN);
    /* dynamic set output logs's filter for tag */
//    elog_set_filter_tag("main");
    /* dynamic set output logs's filter for keyword */
//    elog_set_filter_kw("Hello");
    /* dynamic set output logs's tag filter */
//    elog_set_filter_tag_lvl("main", ELOG_LVL_WARN);

    log_i("EasyLogger Init Done");
}

#ifdef USE_CRYPTOPP
#define LOG_TAG     "md5Check"
void getMD5(byte *md5Str, const size_t MD5SIZE, char *fileName)
{
    CryptoPP::Weak1::MD5 md5;
    CryptoPP::FileSource(fileName, true, new CryptoPP::HashFilter(md5, new CryptoPP::HexEncoder(new CryptoPP::ArraySink(md5Str, MD5SIZE))));
    //reinterpret_cast<const char*>(md5Str), MD5SIZE;
}
#else
#define LOG_TAG     "md5Check"
void readMd5FromFile(FILE * fp, char* filePath, unsigned char* md5Str)
{
    fp = fopen(filePath, "rb");
    if(fp == NULL)
    {
        log_e("md5 result file open failed");
        return;
    }
    fread(md5Str, 32, 1, fp);
    for(int i = 0; i < 32; ++ i)
    {
        md5Str[i] = toupper(md5Str[i]);
    }
    fclose(fp);
    return;
}
#endif

#define LOG_TAG     "shellCheck"
int shellCheck(pid_t status)
{
    if(status == -1)
    {
        log_e("system error");
        return -1;
    }
    else
    {
        if(WIFEXITED(status))
        {
            if(WEXITSTATUS(status) == 0)
            {
                log_d("shell cmd exec done");
            }
            else
            {
                log_e("shell cmd exec error, exit code = %d", WEXITSTATUS(status));
                return -1;
            }
        }
        else
        {
            log_d("exit code = %d", WEXITSTATUS(status));
        }
    }
    return 0;
}

#define LOG_TAG     "ContentCheck"
int elfContentCheck(FILE* fp, char* filePath, char* checkStr)
{
    int fileLen = 0;

    fp = fopen(filePath, "rb");
    if(fp == NULL)
    {
        log_e("content check file open error");
        return 0;
    }
    
    fseek(fp, 0, SEEK_END);
    fileLen = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    if(fileLen >= strlen(checkStr))
    {
        fclose(fp);
        return 1;
    }
    else
    {
        fclose(fp);
        return 0;
    }
}

#define LOG_TAG     "updateFirmware"
int updateDecoderFirmware(char* recvBuf, char* tftpServerIP)
{
    int ret = 0;
    int fail_status = -1;
    pid_t status;
    char fileName[FIRMWARE_NAMELEN] = {0};
    char cmd[CMDLEN] = {0};
    char contentCheckFile[FIRMWARE_PATHLEN] = {0};
#ifdef USE_CRYPTOPP
    const size_t MD5SIZE = CryptoPP::Weak1::MD5::DIGESTSIZE * 4;
    byte md5Str[MD5SIZE] = {0};
    byte md5Ans[MD5SIZE] = {0};
#else
    FILE * fp = NULL;
    char md5ResultFile[FIRMWARE_PATHLEN] = {0};
    unsigned char md5Ans[MD5LEN] = {0};
    unsigned char md5Str[MD5LEN] = {0};
#endif
    log_i("开始升级解码机固件...");

    /* 原固件备份 */
    sprintf(cmd, "mv %s/hytq-vdec-5d %s/hytq-vdec-5d.backup", (char*)APPPATH, (char*)APPPATH);
    status = system(cmd);
    if(!shellCheck(status))
    {
        log_d("firmware backup done");
    }
    else
    {
        log_e("system error, firmware backup failed");
        fail_status = -5;
        return fail_status;
    }

    /* 文件名校验 */
    //strcpy(fileName, "hytq-vdec-5d");       // 后面要换成从数据包里解码的
    strncpy(fileName, recvBuf+4, 12);
    if(!strcmp(fileName, FIRMWARE_FILE))
    {
        log_i("固件文件名校验通过");
    }
    else
    {
        log_e("固件文件名校验失败");
        fail_status = -2;
        goto err;
    }

    /* tftp传输新固件 */
    memset(cmd, 0, sizeof(cmd));
    //sprintf(cmd, "tftp %s -c get %s", tftpServerIP, fileName);      //PC-Ubuntu环境tftp命令
    sprintf(cmd, "tftp -g -r %s %s", fileName, tftpServerIP);     //嵌入式环境tftp命令
    log_d("tftp cmd: %s", cmd);
    status = system(cmd);
    if(!shellCheck(status))
    {
        log_d("tftp transmission done");
    }
    else
    {
        log_e("system error, tftp transmisson failed");
        fail_status = -3;
        goto err;
    }

    /* md5校验 */
    //strcpy((unsigned char*)md5Ans, "0A3CB1DD4F8E2F90EAEFCC88DC754C61"); // 后面要换成从数据包里解码的
    strncpy((char*) md5Ans, recvBuf+16, 32);
#ifdef USE_CRYPTOPP     
    getMD5(md5Str, MD5SIZE, fileName);
#else
    memset(cmd, 0, sizeof(cmd));
    sprintf(cmd, "md5sum %s/hytq-vdec-5d > %s/md5Result", (char*)APPPATH, (char*)APPPATH);
    sprintf(md5ResultFile, "%s/md5Result", (char*)APPPATH);
    status = system(cmd);
    readMd5FromFile(fp, md5ResultFile, md5Str);
#endif
    if(!strcmp((char*) md5Str, (char*) md5Ans))
    {
        log_i("固件MD5校验通过");
    }
    else
    {
        log_e("固件MD5校验失败");
        log_e("正确MD5码:%s", md5Ans);
        log_e("计算MD5码:%s", md5Str);
        fail_status = -4;
        goto err;
    }

    /* 固件内容检查 */
    // 检查字符串1:"receiver_thread_func"
    memset(cmd, 0, sizeof(cmd));
    sprintf(cmd, "grep receiver_thread_func hytq-vdec-5d > %s/contentCheck", (char*)APPPATH, (char*)APPPATH);
    sprintf(contentCheckFile, "%s/contentCheck", (char*)APPPATH);
    status = system(cmd);
    ret = elfContentCheck(fp, contentCheckFile, (char*)"receiver_thread_func");
    if(!ret)
    {
        log_e("固件内容检查失败");
        fail_status = -6;
        goto err;
    }
    // 检查字符串2:"SAMPLE_VDEC_H264"
    memset(cmd, 0, sizeof(cmd));
    sprintf(cmd, "grep SAMPLE_VDEC_H264 hytq-vdec-5d > %s/contentCheck", (char*)APPPATH, (char*)APPPATH);
    sprintf(contentCheckFile, "%s/contentCheck", (char*)APPPATH);
    status = system(cmd);
    ret = elfContentCheck(fp, contentCheckFile, (char*)"SAMPLE_VDEC_H264");
    if(!ret)
    {
        log_e("固件内容检查失败");
        fail_status = -6;
        goto err;
    }    
    log_i("固件内容检查通过");


    /* 更新固件 */
    memset(cmd, 0, sizeof(cmd));
    sprintf(cmd, "chmod 777 %s/hytq-vdec-5d", (char*)APPPATH);
    status = system(cmd);
    log_i("解码机固件升级成功");

    memset(cmd, 0, sizeof(cmd));
    sprintf(cmd, "rm %s/hytq-vdec-5d.backup", (char*)APPPATH);
    status = system(cmd);

    return 0;

err:
    /* 恢复固件 */
    memset(cmd, 0, sizeof(cmd));
    sprintf(cmd, "mv %s/hytq-vdec-5d.backup %s/hytq-vdec-5d", (char*)APPPATH, (char*)APPPATH);
    status = system(cmd);
    log_e("解码机固件升级失败");
    return fail_status;
}

#define LOG_TAG     "transLog"
void transmitLogFile(char* recvBuf, char *tftpServerIP)
{
    pid_t status;
    char cmd[CMDLEN] = {0};
    char logTime[50] = {0};

    strncpy(logTime, recvBuf+4, 19);
    //sprintf(logTime, "%02d%02d_%02d_%02d_%02d_%02d_%02d", (int)recvBuf[4], (int)recvBuf[5], (int)recvBuf[6],\
                                            (int)recvBuf[7], (int)recvBuf[8], (int)recvBuf[9], (int)recvBuf[10]);
    memset(cmd, 0, sizeof(cmd));
    sprintf(cmd, "cp %s/hytq-vdec.log %s/hytq-vdec.log_%s", (char*)LOGPATH, (char*)LOGPATH, logTime);
    status = system(cmd);
    if(!shellCheck(status))
    {
        log_d("copy log file success");
    }

    memset(cmd, 0, sizeof(cmd));
    sprintf(cmd, "chmod 777 %s/hytq-vdec.log_%s", (char*)LOGPATH, logTime);
    status = system(cmd);
    if(!shellCheck(status))
    {
        log_d("chmod 777 log file success");
    }
    sleep(1);

    log_i("开始传输解码机工作日志...");
    memset(cmd, 0, sizeof(cmd));
    //sprintf(cmd, "tftp %s -c put %s/hytq-vdec.log.backup", tftpServerIP, (char*)LOGPATH);
    sprintf(cmd, "tftp -p -l %s/hytq-vdec.log_%s %s", (char*)LOGPATH, logTime, tftpServerIP);  //嵌入式环境tftp传输命令
    log_d("log transmission cmd: %s", cmd);
    status = system(cmd);
    if(!shellCheck(status))
    {
        log_i("解码机工作日志传输成功");
    }
    else
    {
        log_e("解码机工作日志传输失败");
    }
}

#define LOG_TAG     "main"
int main(void) {
    int sockfd = 0;
    int ret = 0;
    struct sockaddr_in servAddr;
    char recvbuf[BUFLEN];
    char sendbuf[BUFLEN];

    /* 日志系统初始化 */
    eLogInit();

    /* TCP通信 */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd < 0)
    {
        log_e("socket failed");
        return EXIT_FAILURE;
    }

    fcntl(sockfd, F_SETFL, O_NONBLOCK);
    bzero(&servAddr, sizeof(servAddr));
	servAddr.sin_family = AF_INET;
	servAddr.sin_port = htons(TCPPORT);
	servAddr.sin_addr.s_addr = htonl(INADDR_ANY);    

    ret = bind(sockfd, (struct sockaddr*)&servAddr, sizeof(struct sockaddr));
    if(ret != 0)
    {
        log_e("bind error, ret = %d", ret);
        return EXIT_FAILURE;
    }

    ret = listen(sockfd, 1);
    if(ret != 0)
    {
        log_e("listen error, ret = %d", ret);
        return EXIT_FAILURE;
    }

    while(1)
    {
        int recvSize = 0;
        char clientIP[INET_ADDRSTRLEN];
        char *pRecv = NULL;
        size_t recvLen = 0;
        size_t sendLen = 0;
        struct sockaddr_in clientAddr;
        struct timeval timeout = {1, 0};
        socklen_t clientAddrLen = sizeof(clientAddr);
        fd_set rdfs;

        int connfd = accept(sockfd, (struct sockaddr*)&clientAddr, &clientAddrLen);
        if(connfd < 0)
        {
            sleep(1);
            log_d("waiting client...");
            continue;
        }

        inet_ntop(AF_INET, &clientAddr.sin_addr.s_addr, clientIP, INET_ADDRSTRLEN);
        log_i("client IP = %s", clientIP);

        while(1)
        {
            FD_ZERO(&rdfs);
            FD_SET(connfd, &rdfs);
            ret = select(connfd+1, &rdfs, NULL, NULL, &timeout);
            if(ret > 0)
            {
                if(FD_ISSET(connfd, &rdfs))
                {
                    recvLen = read(connfd, recvbuf, BUFLEN);
                    recvSize = (int)recvLen;
                    log_d("recvSize = %d", recvSize);
                    if(recvSize > 0)
                    {
                        log_i("receive %lu bytes data", recvLen);
                        pRecv = recvbuf;
                        if(pRecv[0] == 0xab && pRecv[1] == 0xac && pRecv[2] == 0xad && pRecv[3] == 0xae)  //判断帧头，确定是升级固件还是传输日志
                        {
                            ret = updateDecoderFirmware(pRecv, clientIP);
                            memset(sendbuf, 0, sizeof(sendbuf));
                            switch(ret) 
                            {
                                case 0:
                                    sprintf(sendbuf, "解码机固件升级成功，正在自动重启#");
                                    break;
                                case -1:
                                    sprintf(sendbuf, "未知错误，解码机固件升级失败#");
                                    break;
                                case -2:
                                    sprintf(sendbuf, "文件名校验未通过，解码机固件升级失败#");
                                    break;
                                case -3:
                                    sprintf(sendbuf, "固件网络传输失败，解码机固件升级失败#");
                                    break;
                                case -4:
                                    sprintf(sendbuf, "MD5校验未通过，解码机固件升级失败#");
                                    break;
                                case -5:
                                    sprintf(sendbuf, "原固件备份失败，解码机固件升级失败#");
                                    break;
                                case -6:
                                    sprintf(sendbuf, "非解码机固件，解码机固件升级失败#");
                                    break;
                            }
                            while((sendLen = write(connfd, sendbuf, 2048)) <= 0)
                            {
                                static int cntFailed = 0;
                                log_e("send error");
                                ++ cntFailed;
                                if(cntFailed > 10)
                                {
                                    cntFailed = 0;
                                    break;
                                }
                                sleep(1);
                            }
                            if(ret == 0)
                            {
                                system("reboot");
                            }
                        }
                        else if(pRecv[0] == 0xba && pRecv[1] == 0xca && pRecv[2] == 0xda && pRecv[3] == 0xea)
                        {
                            transmitLogFile(pRecv, clientIP);
                        }
                        else if(pRecv[0] == 0xaf && pRecv[1] == 0xbf && pRecv[2] == 0xcf && pRecv[3] == 0xdf)
                        {
                            system("reboot");
                        }
                        else
                        {
                            log_i("invalid info");
                        }
                    }
                    else if(recvSize == 0)
                    {
                        log_i("client disconnect");
                        break;
                    }
                    else 
                    {
                        log_e("server read info failed");
                    }
                }
            }
            else if(ret == 0)
            {
                log_d("select timeout, ret = %d", ret);
                sleep(1);
                continue;
            }
            else
            {
                log_e("select failed");
                break;
            }
        }

        close(connfd);
    }

    return EXIT_SUCCESS;
}

