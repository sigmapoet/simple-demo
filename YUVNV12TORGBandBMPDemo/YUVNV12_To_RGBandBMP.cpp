#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

#define WIDTH 	1920
#define HEIGHT	1080
#define LENGTH	WIDTH*HEIGHT

typedef unsigned char uint8_t;
typedef int LONG;
typedef unsigned int DWORD;
typedef unsigned short WORD;

struct YUVNV12Image {
	uint8_t Y[LENGTH];
	uint8_t U[LENGTH/2];
	uint8_t V[LENGTH/2];		
};

struct RGBImage {
	uint8_t R[LENGTH];
	uint8_t G[LENGTH];
	uint8_t B[LENGTH];
};
 
typedef struct {
        WORD    bfType;
        DWORD   bfSize;
        WORD    bfReserved1;
        WORD    bfReserved2;
        DWORD   bfOffBits;
} BMPFILEHEADER_T;
 
typedef struct{
        DWORD      biSize;
        LONG       biWidth;
        LONG       biHeight;
        WORD       biPlanes;
        WORD       biBitCount;
        DWORD      biCompression;
        DWORD      biSizeImage;
        LONG       biXPelsPerMeter;
        LONG       biYPelsPerMeter;
        DWORD      biClrUsed;
        DWORD      biClrImportant;
} BMPINFOHEADER_T;
 
int savebmp(unsigned char * pdata, char * bmp_file, int width, int height) {      //分别为rgb数据，要保存的bmp文件名，图片长宽
       int size = width*height*3*sizeof(char); // 每个像素点3个字节
       // 位图第一部分，文件信息
       BMPFILEHEADER_T bfh;
       bfh.bfType = (WORD)0x4d42;  //bm
       bfh.bfSize = size  // data size
              + sizeof( BMPFILEHEADER_T ) // first section size
              + sizeof( BMPINFOHEADER_T ) // second section size
              ;
       bfh.bfReserved1 = 0; // reserved
       bfh.bfReserved2 = 0; // reserved
       bfh.bfOffBits = 54;//真正的数据的位置
       //bfh.bfOffBits = sizeof(BMPFILEHEADER_T) + sizeof(BMPINFOHEADER_T);
       // 位图第二部分，数据信息
       BMPINFOHEADER_T bih;
       bih.biSize = sizeof(BMPINFOHEADER_T);
       bih.biWidth = width;
       bih.biHeight = -height;//BMP图片从最后一个点开始扫描，显示时图片是倒着的，所以用-height，这样图片就正了
       bih.biPlanes = 1;//为1，不用改
       bih.biBitCount = 24;
       bih.biCompression = 0;//不压缩
       bih.biSizeImage = size;
       bih.biXPelsPerMeter = 2835 ;//像素每米
       bih.biYPelsPerMeter = 2835 ;
       bih.biClrUsed = 0;//已用过的颜色，24位的为0
       bih.biClrImportant = 0;//每个像素都重要
       
       FILE * fp = fopen(bmp_file,"wb" );
       if( !fp ) return -1;
 
       fwrite(&bfh, 8, 1,  fp );//由于linux上4字节对齐，而信息头大小为54字节，第一部分14字节，第二部分40字节，所以会将第一部分补齐为16自己，直接用sizeof，打开图片时就会遇到premature end-of-file encountered错误
       fwrite(&bfh.bfReserved2, sizeof(bfh.bfReserved2), 1, fp);
       fwrite(&bfh.bfOffBits, sizeof(bfh.bfOffBits), 1, fp);
       fwrite(&bih, sizeof(BMPINFOHEADER_T), 1, fp);
       fwrite(pdata, size, 1, fp);
       fclose( fp );
       
       return 0;
}

int ReadImage(char* filePath, struct YUVNV12Image* yuvImage) {
	FILE* fp = NULL;
	int ret;
	int Ucount = 0, Vcount = 0;
	static uint8_t UV[LENGTH/2];
	
	fp = fopen(filePath, "rb+");
	if(fp == NULL) {
		perror("image file fopen error");
		return -1;
	}	
	
	ret = fread(yuvImage->Y, 1, LENGTH, fp);
	if(ret < LENGTH) {
		if(ferror(fp)) {
			printf("fread error\n");
			fclose(fp);
			return -1;
		}
	}
	
	memset(UV, 0, sizeof(UV));
	ret = fread(UV, 1, LENGTH/2, fp);
	if(ret < LENGTH) {
		if(ferror(fp)) {
			printf("fread error\n");
			fclose(fp);
			return -1;
		}
	}
	for(int i = 0; i < LENGTH/2; ++ i) {
		if(i & 1) {
			yuvImage->V[Vcount] = UV[i];
			Vcount ++;
		}
		else {
			yuvImage->U[Ucount] = UV[i];
			Ucount ++;
		}
	}
	
	fclose(fp);
	return 0;
}

void CalculateYUVToRGB(uint8_t Y, uint8_t U, uint8_t V, uint8_t* R, uint8_t* G, uint8_t* B) {
	*R = Y + 1.402 * (V-128);
	*G = Y - 0.344 * (U-128) - 0.714 * (V-128);
	*B = Y + 1.772 * (U-128);
	//*R = Y;
	//*G = Y;
	//*B = Y;
}

int ConvertYUVNV12ToRGB(struct YUVNV12Image* yuvImage, struct RGBImage* rgbImage) {
	int groupY;
	int pixelOrder;
	
	for(int i = 0; i < HEIGHT; ++ i) {
		for(int j = 0; j < WIDTH; ++ j) {
			//printf("i : %d, j : %d, groupY : %d\n", i, j, groupY);
			groupY = (i / 2) * (WIDTH/2) + (j / 2);
			pixelOrder = i * WIDTH + j;
			CalculateYUVToRGB(yuvImage->Y[pixelOrder], yuvImage->U[groupY], yuvImage->V[groupY], \
							&(rgbImage->R[pixelOrder]), &(rgbImage->G[pixelOrder]), &(rgbImage->B[pixelOrder]));		
		}
	}
		
	return 0;
}

int SaveBMPImage(struct RGBImage* rgbImage, char *savePath) {
	int ret;
	int totalBytes = 0;
	FILE* fp = NULL;
	static uint8_t rgbData[LENGTH * 3];
	static char bmpData[LENGTH * 3 + 54];
	memset(rgbData, 0, sizeof(rgbData));
	
	for(int i = 0; i < LENGTH; ++ i) {
		rgbData[totalBytes ++] = rgbImage->B[i];
		rgbData[totalBytes ++] = rgbImage->G[i];
		rgbData[totalBytes ++] = rgbImage->R[i];
	}

	ret = savebmp(rgbData, savePath, WIDTH, HEIGHT);
	if(ret) {
		printf("BGR888 TO BMP Error\n");
		return -1;
	}
	return 0;
}

int SaveRGBImage(struct RGBImage* rgbImage) {
	FILE* fp = NULL;
	int ret;
	
	fp = fopen("pic.rgb", "wb+");
	if(fp == NULL) {
		perror("RGB Image File fopen error");
		return -1;
	}	
	
	ret = fwrite(rgbImage->R, 1, LENGTH, fp);
	if(ret != LENGTH) {
		perror("R data fwrite error");
		fclose(fp);
		return -1;
	}
	ret = fwrite(rgbImage->G, 1, LENGTH, fp);
	if(ret != LENGTH) {
		perror("G data fwrite error");
		fclose(fp);
		return -1;
	}
	ret = fwrite(rgbImage->B, 1, LENGTH, fp);
	if(ret != LENGTH) {
		perror("B data fwrite error");
		fclose(fp);
		return -1;
	}
	
	fclose(fp);
	return 0;
}

int main(int argc, char** argv) {
	
	if(argc != 3) {
		fprintf(stderr, "please input yuv image path and output bmp file path\n");
		fprintf(stderr, "usage: %s /home/pic/pic1.yuv  /home/pic/pic1.bmp\n", argv[0]);
		return 0;
	}
		
	int ret;
	static struct YUVNV12Image yuvImage;
	static struct RGBImage rgbImage;
	/*
	char filePath[100];
	
	memset(filePath, 0, sizeof(filePath));
	sprintf(filePath, "C:\\Users\\SigmaPoet\\Desktop\\pic3.yuv");
	*/
	//printf("1\n");
	ret = ReadImage(argv[1], &yuvImage);
	if(ret) {
		printf("ReadImage Error\n");
		return -1;
	}
	//printf("2\n");
	ret = ConvertYUVNV12ToRGB(&yuvImage, &rgbImage);
	if(ret) {
		printf("Convert YUV to RGB Error\n");
		return -1;
	}
	
	/*
	ret = SaveRGBImage(&rgbImage);
	if(ret) {
		printf("Save RGB Image Error\n");
		return -1;
	}
	*/
	ret = SaveBMPImage(&rgbImage, argv[2]);
	if(ret) {
		printf("Save BMP Image Error\n");
		return -1;
	}
	//printf("3\n");
	printf("Convert Done\n");
	return 0;
}
 
