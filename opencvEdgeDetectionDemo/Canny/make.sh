#!/bin/bash

arm-himix200-linux-g++ cannyDemo.cpp -o cannyDemo -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_contrib -lopencv_flann -lopencv_features2d -lopencv_gpu -lopencv_legacy -lopencv_nonfree -lopencv_objdetect -lopencv_calib3d -lopencv_ml -lopencv_ocl -lopencv_photo -lopencv_stitching -lopencv_superres -lopencv_video -lopencv_videostab -Ofast

