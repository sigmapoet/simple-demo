#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <time.h>

using namespace std;
using namespace cv;

struct timespec timeStart = {0, 0};
struct timespec timeEnd = {0, 0};

#define ull unsigned long long
#define time_ms_process (ull)(timeEnd.tv_sec*1000+timeEnd.tv_nsec/1000000) - (ull)(timeStart.tv_sec*1000+timeStart.tv_nsec/1000000);

int main() {
	Mat img = imread("dstImage.bmp");
	Mat dstPic, edge, grayImage;
	ull durTimeProcess = 0;

	imwrite("原始图.bmp", img);
	
	dstPic.create(img.size(), img.type());
	cvtColor(img, grayImage, COLOR_BGR2GRAY);
	blur(grayImage, edge, Size(3, 3));
	
	clock_gettime(CLOCK_REALTIME, &timeStart);
	
	Canny(edge, edge, 50, 150, 3);

	clock_gettime(CLOCK_REALTIME, &timeEnd);
	durTimeProcess = time_ms_process;
	printf("canny处理时间: %llu ms\n\r", durTimeProcess);

	imwrite("边缘提取效果.bmp", edge);

	return 0;
}
