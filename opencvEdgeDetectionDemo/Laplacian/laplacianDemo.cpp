#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <time.h> 
using namespace std;
using namespace cv;

struct timespec timeStart = {0, 0};
struct timespec timeEnd = {0, 0};

#define ull unsigned long long
#define time_ms_process (ull)(timeEnd.tv_sec*1000+timeEnd.tv_nsec/1000000) - (ull)(timeStart.tv_sec*1000+timeStart.tv_nsec/1000000);

int main() {
	Mat img = imread("dstImage.bmp");
	Mat gray, dst, abs_dst;
	ull durTimeProcess = 0;
	
	imwrite("原始图.bmp", img);
	
	GaussianBlur(img, img, Size(3, 3), 0, 0, BORDER_DEFAULT);
	cvtColor(img, gray, COLOR_RGB2GRAY);

	clock_gettime(CLOCK_REALTIME, &timeStart);
	
	Laplacian(gray, dst, CV_16S, 3, 1, 0, BORDER_DEFAULT);
	convertScaleAbs(dst, abs_dst);
	
	clock_gettime(CLOCK_REALTIME, &timeEnd);
	durTimeProcess = time_ms_process;
	printf("laplacian处理时间: %llu ms\n\r", durTimeProcess);
		
	imwrite("边缘提取效果.bmp", abs_dst);

	return 0;
}
