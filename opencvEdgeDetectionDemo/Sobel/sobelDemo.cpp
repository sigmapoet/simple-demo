#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <time.h>

using namespace std;
using namespace cv;

struct timespec timeStart = {0, 0};
struct timespec timeEnd = {0, 0};

#define ull unsigned long long
#define time_ms_process (ull)(timeEnd.tv_sec*1000+timeEnd.tv_nsec/1000000) - (ull)(timeStart.tv_sec*1000+timeStart.tv_nsec/1000000);

int main() {
	Mat img = imread("dstImage.bmp");
	ull durTimeProcess = 0;
	
	imwrite("原始图.bmp", img);

	Mat grad_x, grad_y;
	Mat abs_grad_x, abs_grad_y, dst;

	clock_gettime(CLOCK_REALTIME, &timeStart);
	
	Sobel(img, grad_x, CV_16S, 1, 0, 3, 1, 1, BORDER_DEFAULT);
	convertScaleAbs(grad_x, abs_grad_x);
	//imwrite("x方向sobel.bmp", abs_grad_x);

	Sobel(img, grad_y, CV_16S, 0, 1, 3, 1, 1, BORDER_DEFAULT);
	convertScaleAbs(grad_y, abs_grad_y);
	//imwrite("y方向sobel.bmp", abs_grad_y);

	addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, dst);

	clock_gettime(CLOCK_REALTIME, &timeEnd);
	durTimeProcess = time_ms_process;
	printf("sobel处理时间: %llu ms\n\r", durTimeProcess);
	
	imwrite("整体方向sobel.bmp", dst);

	return 0;
}
