import cv2

class HikVision(object):
    _defaults = {
        "url": "rtsp://admin:Hik130130130@192.168.0.64:554/h264/ch1/main/av_stream"
    }

    @classmethod
    def get_defaults(cls, n):
        if n in cls._defaults:
            return cls.get_defaults[n]
        else:
            return "Unrecognized attribute name'" + n + "'"
    
    def __init__(self, **kwargs):
        self.__dict__.update(self._defaults)
        self.__dict__.update(**kwargs)

        self.cap = cv2.VideoCapture(self.__dict__['url'])

    def check_status(self):
        if self.cap.isOpened():
            rval, frame = self.cap.read()
            return rval
        else:
            self.cap.open(self.__dict__['url'])
            if self.cap.isOpened():
                rval, frame = self.cap.read()
                return rval
            else:
                rval = False
                return rval
    
    def get_image(self):
        rval, frame = self.cap.read()
        if rval:
            return frame
        else:
            status = self.check_status()
            if status:
                rval, frame = self.cap.read()
                if rval:
                    return frame
                else:
                    return None
            else:
                return None

    def stop_capture(self):
        self.cap.release()

if __name__ == "__main__":
    cam = HikVision()
    while 1:
        frame = cam.get_image()
        if frame is not False:
            frame = cv2.resize(frame, (1280, 720))
            cv2.imshow("imshow", frame)
            key = cv2.waitKey(1)
            if key == 27:
                break
    cam.stop_capture()
    cv2.destroyAllWindows()
                        
