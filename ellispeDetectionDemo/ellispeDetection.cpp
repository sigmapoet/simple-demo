/*
	版本：			1.6
	描述：			在1.5版本基础上进行了优化：
				1. 固定canny的阈值为50和150。
				优化导致的结果：
				2. (新数据集) 优化后算法在hi3519av100上的性能：平均图片处理时间18~19ms，平均处理帧率53~54fps。 
				额外测试：
				3. 分别在hi3519av100和hi3516dv300上对海思IVE的canny进行了测试，处理640*480的灰度图，需要45ms左右，
				直接使用opencv的canny算法，用时16ms左右。
	修改日期：		2022/3/9
*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>
#include <limits.h>
#include <float.h>
#include <iostream>
#include "opencv2/core/core.hpp" 
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <time.h>
#include <algorithm>
#include <numeric>


using namespace std;
using namespace cv;

#define pi 3.141592653589793

//#ifndef __OPENCV_CONTRIB_HPP__

//#define __OPENCV_CONTRIB_HPP__

//class CV_EXPORTS Directory
//{
//	public:
//		static std::vector<std::string> GetListFiles(const std::string& path, const std::string &exten="*", bool addPath=true);
//		static std::vector<std::string> GetListFilesR(const std::string& path, const std::string &exten="*", bool addPath=true);
//		static std::vector<std::string> GetListFolders(const std::string& path, const std::string &exten="*", bool addPath=true);
//};

//#endif

/* ********************** Debug Switch ************************** */

//#define myTimer			//代码不同环节计时开关
//#define log			//代码执行所到行数标记开关
//#define parametersLog 	//输出参数值开关

/* ************************************************************** */

/* ********************  Debug Variables ************************* */

double averageProcessTime = 0;
double averageProcessFrameRate = 0;
int totalImageCount;
/* *************************************************************** */

float sigma;
float RD_input;
float Rd_input;
float TScore_input;

struct trans//定义结构体
{
	int x;
	int y;
	int num;
};

struct Data_Parameters//定义数据
{
	vector<vector<Point>> data_ori;//原始坐标数据
	vector<vector<Point2f>> data_gauss;//高斯演化数据
	vector<vector<double>> K;//曲率
	vector<double> mean_k;//平均曲率
	vector<int> data_num;//曲线的边缘点数量
	vector<double> curve_deep;//曲线的深度
	vector<double> ratation_angle;//曲线的旋转角度（顺时针）
	vector<vector<double>> angle;//曲线端点处切线角度
};

struct CandidatesPara//定义数据
{
	vector<vector<int>> Group;//组合
	vector<int> Num;
	vector<vector<double>> ellipses;//椭圆参数
	vector<double> Score;//得分
};

trans Findnum(Mat src,int n,int r)//numFound=0
{
	int rows = src.rows;
	int cols = src.cols;
	int N=0;
	trans A;
	A.num = 0;
	for (int i = r; i < rows; i++)
	{
		if(N==1)
			break;
		else
		{
			uchar* data = src.ptr<uchar>(i);
			for (int j = 0; j < cols; j++)
			{
				if (data[j]>n)
				{
					A.x = j;
					A.y = i;
					A.num = 1;
					N=1;
				}
			}
		}
	}
	return A;
}


double round(double r)
{
    return (r > 0.0) ? floor(r + 0.5) : ceil(r - 0.5);
}
//定义距离函数
//new
trans IndexMinDistance(Mat src,int numFound)//numFound=1
{
	trans A;
	A.num = 0;
	if(src.ptr<uchar>(0)[1]>numFound)
	{A.x = 1;A.y = 0;A.num=1;}
	else if(src.ptr<uchar>(1)[0]>numFound)
	{A.x = 0;A.y = 1;A.num=1;}
	else if(src.ptr<uchar>(1)[2]>numFound)
	{A.x = 2;A.y = 1;A.num=1;}
	else if(src.ptr<uchar>(2)[1]>numFound)
	{A.x = 1;A.y = 2;A.num=1;}
	else if(src.ptr<uchar>(0)[0]>numFound)
	{A.x = 0;A.y = 0;A.num=1;}
	else if(src.ptr<uchar>(0)[2]>numFound)
	{A.x = 2;A.y = 0;A.num=1;}
	else if(src.ptr<uchar>(2)[0]>numFound)
	{A.x = 0;A.y = 2;A.num=1;}
	else if(src.ptr<uchar>(2)[2]>numFound)
	{A.x = 2;A.y = 2;A.num=1;}
	return A;
}


//归一化函数
double *Norm(double *V)
{
	double d;
	d = pow(pow(V[0],2.0)+pow(V[1],2.0),0.5);
	double Ve[2];
	
	Ve[0] = V[0]/d;
	Ve[1] = V[1]/d;
	return Ve;
}//1111
//point_distribution
double point_distribution( vector<Point2f> data,double k,double b )
{
	double ratio;
	int n1 = 0;
	int n2 = 0;
	for(int i=0;i<data.size();i++)
	{
		Point2f p = data[i];
		if(p.y>k*p.x+b)
		{n1=n1+1;}
		else
		{n2=n2+1;}
	}
	ratio = double(min(n1,n2))/double(max(n1,n2));
	return ratio;
}
double PointDistribution( vector<Point> data,double k,double b )//Point版本
{
	double ratio;
	int n1 = 0;
	int n2 = 0;
	for(int i=0;i<data.size();i++)
	{
		Point p = data[i];
		if(p.y>=k*p.x+b)
		{n1=n1+1;}
		else
		{n2=n2+1;}
	}
	ratio = double(min(n1,n2))/double(max(n1,n2));
	return ratio;
}
double PointDistributionT( vector<Point> data,double k,double b )//Point版本
{
	double ratio;
	int n1 = 0;
	int n2 = 0;
	for(int i=0;i<data.size();i++)
	{
		Point p = data[i];
		if(p.x>k*p.y+b)
		{n1=n1+1;}
		else
		{n2=n2+1;}
	}
	ratio = double(min(n1,n2))/double(max(n1,n2));
	return ratio;
}
double NANDistribution( vector<Point> data,int x0)//斜率无穷-版本
{
	double ratio;
	int n1 = 0;
	int n2 = 0;
	for(int i=0;i<data.size();i++)
	{
		Point p = data[i];
		if(p.x>x0)
		{n1=n1+1;}
		else
		{n2=n2+1;}
	}
	ratio = double(min(n1,n2))/double(max(n1,n2));
	return ratio;
}
//符号函数
double Sign_double(double x)
{
	if(x>0)
		return 1;
	else if(x<0)
		return -1;
	else
		return 0;
}
//计算向量余弦
/*double GetcosT(double *v1,double *v2)
{
	double t;
	t = v1[0]*v2[0]+v1[1]*v2[1];
	return t;
}*/

//卷积
vector<Point2f> conv(vector<Point> Cur,double *gau,int num,int N)
{
	vector<Point2f> Cur_gauss(num);
	for(int i=0;i<num;i++)
	{
		Cur_gauss[i].x=0;
		Cur_gauss[i].y=0;;
		for(int j=0;j<2*N+1;j++)
		{
			Cur_gauss[i].x =  Cur_gauss[i].x+Cur[i+j].x*gau[j];
			Cur_gauss[i].y =  Cur_gauss[i].y+Cur[i+j].y*gau[j];
		}
	}
	return Cur_gauss;
}

//曲率计算
vector<double> K_Calculate(vector<Point2f> cur_gauss,double *mean_k)
{
	int num = cur_gauss.size();
	double *Xu;
	Xu = new double[num-2];
	double *Yu;
	Yu = new double[num-2];
	for(int i=2;i<num;i++)
	{
		Xu[i-2] = (cur_gauss[i].x-cur_gauss[i-2].x)/2;
		Yu[i-2] = (cur_gauss[i].y-cur_gauss[i-2].y)/2;
	}

	double *Xuu;
	Xuu = new double[num-4];
	double *Yuu;
	Yuu = new double[num-4];
	for(int i=2;i<num-2;i++)
	{
		Xuu[i-2] = (Xu[i]-Xu[i-2])/2;
		Yuu[i-2] = (Yu[i]-Yu[i-2])/2;
	}
	
	vector<double> K(num);
	for(int i=2;i<num-2;i++)
	{
		K[i] = abs((Xu[i-1]*Yuu[i-2]-Xuu[i-2]*Yu[i-1])/(pow(Xu[i-1]*Xu[i-1]+Yu[i-1]*Yu[i-1],1.5)));
		*mean_k = *mean_k+K[i];
	}
	K[0] = K[2];
	K[1] = K[2];
	K[num-2] = K[num-3];
	K[num-1] = K[num-3];
	*mean_k = (*mean_k+2*K[2]+2*K[num-3])/num;
	return K;
}

//连续边缘提取函数
vector<vector<Point>> extract_curve(Mat grayImage,double TL,int * cur_num)
{
	int L = grayImage.rows;//行
	int W = grayImage.cols;//列
	int Gap_size = 1;//边缘链接的最小间隔
	Mat BW = grayImage;
	Mat BW1 = Mat::zeros(grayImage.rows+2*Gap_size,grayImage.cols+2*Gap_size, CV_8UC1);//灰度图像	
	//imwrite("cannyTest.bmp", BW);
	BW.copyTo(BW1(Rect(Gap_size,Gap_size,W,L)));//Rect(起始列，起始行，列数，行数)
	//int cur_num = 0;
	trans A;
	A = Findnum(BW1,0,0);//numFound>0
	vector<vector<Point>> curve;//数据类型
	vector<Point> neighbour;
	while(A.num>0)
	{
		Point point_seed;
		point_seed.x = A.x;
		point_seed.y = A.y;

		vector<Point> cur;
		cur.push_back(point_seed);
		BW1.at<uchar>(point_seed.y,point_seed.x)=0;
		trans MinNeighbour;
		MinNeighbour = IndexMinDistance(BW1(Rect(point_seed.x-Gap_size,point_seed.y-Gap_size,3,3)),0);
		while(MinNeighbour.num>0)
		{
			Point point_i = point_seed;
			point_seed.x = point_i.x+MinNeighbour.x-Gap_size;
			point_seed.y = point_i.y+MinNeighbour.y-Gap_size;
			cur.push_back(point_seed);
			BW1.at<uchar>(point_seed.y,point_seed.x)=0;
			MinNeighbour = IndexMinDistance(BW1(Rect(point_seed.x-Gap_size,point_seed.y-Gap_size,3,3)),0);
		}

		point_seed.x = A.x;
		point_seed.y = A.y;
		BW1.at<uchar>(point_seed.y,point_seed.x)=0;
		MinNeighbour = IndexMinDistance(BW1(Rect(point_seed.x-Gap_size,point_seed.y-Gap_size,3,3)),0);
		while(MinNeighbour.num>0)
		{
			Point point_i = point_seed;
			point_seed.x = point_i.x+MinNeighbour.x-Gap_size;
			point_seed.y = point_i.y+MinNeighbour.y-Gap_size;
			cur.insert(cur.begin(),point_seed);
			BW1.at<uchar>(point_seed.y,point_seed.x)=0;
			MinNeighbour = IndexMinDistance(BW1(Rect(point_seed.x-Gap_size,point_seed.y-Gap_size,3,3)),0);
		}

		if(cur.size()>TL)
		{
			curve.push_back(cur);
			*cur_num = *cur_num+1;	
		}
		A = Findnum(BW1,0,A.y);	
	}
	return curve;
}

//高斯演化全过程
vector<Point2f> GaussData(vector<Point> cur,double sigma)
{
    int num = cur.size();
	int half_num = ceil(double(num)/2.0);
	int N = min(half_num,10);
	//生成高斯核
	double *gau;
	gau = new double[2*N+1];
	double sum_gau = 0;
	for(int s=-N;s<N+1;s++)
	{
		gau[s+N] = 1/(sigma*pow(2*pi,0.5))*exp(-s*s/(2*sigma*sigma));
		sum_gau = sum_gau+gau[s+N];
	}
	for(int s=0;s<2*N+1;s++)
	{
		gau[s] = gau[s]/sum_gau;
	}
	//生成数据
	vector<Point> loop1(N);
	loop1.assign(cur.begin(), cur.begin()+N);
	vector<Point> loop2(N);
	loop2.assign(cur.end()-N, cur.end());

	vector<Point> line1=loop1;
	reverse(line1.begin(),line1.end());
	vector<Point> line2=loop2;
	reverse(line2.begin(),line2.end());

	int Num = 2*num;
	vector<Point> Cur(Num);
	Cur.assign(line1.begin(), line1.end());
	Cur.insert(Cur.end(),cur.begin(), cur.end());
	Cur.insert(Cur.end(),line2.begin(), line2.end());
	//卷积
	vector<Point2f> cur_gauss(num);//卷积后的坐标
	cur_gauss = conv(Cur,gau,num,N);
	return cur_gauss;
}

//闭合曲线分类
void Curve_Classify(vector<vector<Point>> curve,int cur_num,Data_Parameters *Edgeline,Data_Parameters *Edgeloop)
{
	//判断闭合:0闭合
	int *Line_Loop;//是否闭合
	Line_Loop = new int[cur_num];
	int numline = 0;
	int numloop = 0;
	for(int i=0;i<cur_num;i++)
	{
		if(pow(curve[i][0].x-curve[i][curve[i].size()-1].x,2.0)+pow(curve[i][0].y-curve[i][curve[i].size()-1].y,2.0)<=2)
		{Line_Loop[i]=0;numloop++;}
		else
		{Line_Loop[i]=1;numline++;}
	}
	//高斯演化
	int numlineedge = 0;
	int numloopedge = 0;
	//double sigma =3;
	for(int i=0;i<cur_num;i++)
	{
		vector<Point> cur;
		cur = curve[i];
		int num = cur.size();
		int half_num = ceil(double(num)/2.0);
		int N = min(half_num,6); //原来是10
		//生成高斯核
		double *gau;
		gau = new double[2*N+1];
		double sum_gau = 0;
		for(int s=-N;s<N+1;s++)
		{
			gau[s+N] = 1/(sigma*pow(2*pi,0.5))*exp(-s*s/(2*sigma*sigma));
			sum_gau = sum_gau+gau[s+N];
		}
		for(int s=0;s<2*N+1;s++)
		{
			gau[s] = gau[s]/sum_gau;
		}

		//生成数据
		vector<Point> loop1(N);
		loop1.assign(cur.begin(), cur.begin()+N);
		vector<Point> loop2(N);
		loop2.assign(cur.end()-N, cur.end());

		vector<Point> line1=loop1;
		reverse(line1.begin(),line1.end());
		vector<Point> line2=loop2;
		reverse(line2.begin(),line2.end());

		int Num = 2*num;
		vector<Point> Cur(Num);
		if(Line_Loop[i]==1)//非闭合曲线
		{
			Edgeline->data_ori.push_back(cur);
			Edgeline->data_num.push_back(num);
			Cur.assign(line1.begin(), line1.end());
			Cur.insert(Cur.end(),cur.begin(), cur.end());
			Cur.insert(Cur.end(),line2.begin(), line2.end());
			//卷积
			vector<Point2f> cur_gauss(num);//卷积后的坐标
			cur_gauss = conv(Cur,gau,num,N);
			Edgeline->data_gauss.push_back(cur_gauss);
			//曲率计算
			vector<double> k(num);
			double mean_k=0;//平均曲率
			k = K_Calculate(cur_gauss,&mean_k);
			Edgeline->K.push_back(k);
			Edgeline->mean_k.push_back(mean_k);
			numlineedge++;
		}
		else//闭合曲线
		{
			Edgeloop->data_ori.push_back(cur);
			Edgeloop->data_num.push_back(num);
			Cur.assign(loop2.begin(), loop2.end());
			Cur.insert(Cur.end(),cur.begin(), cur.end());
			Cur.insert(Cur.end(),loop1.begin(), loop1.end());
			//卷积
			vector<Point2f> cur_gauss(num);//卷积后的坐标
			cur_gauss = conv(Cur,gau,num,N);
			Edgeloop->data_gauss.push_back(cur_gauss);
			//曲率计算
			vector<double> k(num);
			double mean_k=0;//平均曲率
			k = K_Calculate(cur_gauss,&mean_k);
			Edgeloop->K.push_back(k);
			Edgeloop->mean_k.push_back(mean_k);
			numloopedge++;
		}
	}
}

//椭圆拟合
void FIT_Ellipses(vector<Point> Contours,vector<double> &ellipse_5D,double pe[6],double *Det)
{
	RotatedRect box;	
	box = fitEllipse(Contours);
	ellipse_5D[0] = box.size.height/2;
	ellipse_5D[1] = box.size.width/2;
	ellipse_5D[2] = box.center.x;
	ellipse_5D[3] = box.center.y;
	ellipse_5D[4] = box.angle*pi/180-pi/2;//短轴与X正向夹角

	double a,b,x0,y0,theta;
	a  = ellipse_5D[0];
	b  = ellipse_5D[1];
	x0  = ellipse_5D[2];
	y0  = ellipse_5D[3];
	theta  = ellipse_5D[4]+pi;//问题
	//参数方程转换一般方程
	pe[5] = pow(b,2)*pow((x0*cos(theta)+y0*sin(theta)),2)+pow(a,2)*pow((x0*sin(theta)-y0*cos(theta)),2)-pow(a,2)*pow(b,2);
	pe[0] = (pow(b,2)*pow(cos(theta),2)+pow(a,2)*pow(sin(theta),2))/pe[5];
    pe[1] = (2*sin(theta)*cos(theta)*(pow(b,2)-pow(a,2)))/pe[5];
    pe[2] = (pow(b,2)*pow(sin(theta),2)+pow(a,2)*pow(cos(theta),2))/pe[5];
    pe[3] = (-2*(pow(b,2)*pow(cos(theta),2)+pow(a,2)*pow(sin(theta),2))*x0-(2*sin(theta)*cos(theta)*(pow(b,2)-pow(a,2)))*y0)/pe[5];
    pe[4] = (-2*(pow(b,2)*pow(sin(theta),2)+pow(a,2)*pow(cos(theta),2))*y0-(2*sin(theta)*cos(theta)*(pow(b,2)-pow(a,2)))*x0)/pe[5];
    pe[5] = 1;

	*Det = pow(pe[1],2)-4*pe[0]*pe[2];

}

//拟合误差
double * DetPix(vector<Point> data,Point2f center,double p[6],int M,double det_inner,double *ratio_inner,double *ratio_H)
{
	double *Error;
	Error = new double[M];
	double inner = 0;
	double inner_H = 0;
	for(int i=0;i<M;i++)
	{
		double sj[2];
		sj[0] = data[i].x;
		sj[1] = data[i].y;
		double m;
		m = (center.y-sj[1])/(center.x-sj[0]);
		double n;
		n = center.y - m*center.x;
		double a,b,c,det,x1,y1,x2,y2,det_pixi;
		Point2f p1,p2;
		a = p[0]+m*p[1]+m*m*p[2];
		b = n*p[1]+2*m*n*p[2]+p[3]+m*p[4];
		c = p[2]*n*n+n*p[4]+p[5];
		det = b*b-4*a*c;
		x1 = (-b-pow(det,0.5))/(2*a);
		y1 = m*x1+n;
		p1.x = x1;
		p1.y = y1;

		x2 = (-b+pow(det,0.5))/(2*a);
		y2 = m*x2+n;
		p2.x = x2;
		p2.y = y2;
		det_pixi = min(pow(pow(sj[0]-p1.x,2.0)+pow(sj[1]-p1.y,2.0),0.5),pow(pow(sj[0]-p2.x,2.0)+pow(sj[1]-p2.y,2.0),0.5));
		if(det_pixi<det_inner)
		{
			inner = inner+1;
		}
		else if(det_pixi> 1.5*det_inner)
		{
			inner_H = inner_H+1;
		}
		Error[i] = det_pixi;
	}
	*ratio_inner = inner/M;
	*ratio_H = inner_H/M;
	return Error;
}

//拟合+筛选
void FitAndSelect(vector<double> ellipse_5D,vector<vector<double>> &f_5D,int *num_ellipse,vector<Point> data,double pe[6],int num,double det_inner,int *S)
{
	Point2f center;
	center.x = ellipse_5D[2];
	center.y = ellipse_5D[3];
	double *error_data;
	double ratio_inner,ratio_H;
	error_data = DetPix(data,center,pe,num,det_inner,&ratio_inner,&ratio_H);
	if(ratio_inner>Rd_input && ratio_H<RD_input)
	{
		f_5D.push_back(ellipse_5D);
		*num_ellipse = *num_ellipse+1;
		*S = 1;
	}
}

//极大值索引
vector<int> ExtremumMaxDetection(vector<double> k,double T)
{
	vector<double> Lmax1(k.size()-1);
	for(int i=0;i<k.size()-1;i++)
	{
		Lmax1[i] = Sign_double(k[i+1]-k[i]);
	}

	vector<double> Lmax2(k.size());
	for(int i=0;i<k.size()-2;i++)
	{
		double a=Lmax1[i+1]-Lmax1[i];
		Lmax2[i+1] = Lmax1[i+1]-Lmax1[i];
		if(Lmax2[i+1]==-1.0 || Lmax2[i+1]==-2.0)
		{
			Lmax2[i+1]=1;
		}
		else Lmax2[i+1]=0;
	}
	Lmax2[0] = 0;
	Lmax2[k.size()-1] = 0;

	vector<int> index;
	for(int i=0;i<k.size();i++)
	{
		if(Lmax2[i]==1.0 && k[i]>=T )
		{
			index.push_back(i);
		}
	}
	return index;
}

//角点位置索引
vector<vector<int>> CornersDetection(vector<vector<double>> K,vector<int> data_num,int cur_num)
{
	vector<vector<int>> corners_index;
	vector<int> index;
	for(int i=0;i<cur_num;i++)
	{
		double T = min(0.12,max(0.09/pow((double(data_num[i])/150.0),0.5),0.07));
		index = ExtremumMaxDetection(K[i],T);
		corners_index.push_back(index);
	}
	return corners_index;
}

//计算均值
double Mean(vector<double> K)
{
	double sum = 0;
	for(int i=0;i<K.size();i++)
	{
		sum = sum+K[i];
	}
	sum = sum/K.size();
	return sum;
}
//计算距离
double Norm(Point2f p1,Point2f p2)
{
	double dis;
	dis = pow(pow(double(p1.x-p2.x),2.0)+pow(double(p1.y-p2.y),2.0),0.5);
	return dis;
}
//曲线断开
Data_Parameters BreakCurve(Data_Parameters Edgeline,vector<vector<int>> corners_index,int cur_num,double TL,double Tline)
{
	Data_Parameters Segments;
	for(int i=0;i<cur_num;i++)
	{
		vector<int> index = corners_index[i];
		int num = index.size()+1;
		int n = 3;
		if(num==1)//无角点
		{
			if(Edgeline.mean_k[i]>Tline)//非直线筛选
			{
				Point p1 = Edgeline.data_ori[i][0];
				Point p2 = Edgeline.data_ori[i][Edgeline.data_ori[i].size()-1];
				Point p3 = Edgeline.data_ori[i][round(double(Edgeline.data_ori[i].size())/2.0)-1];
				Point2f pmid;
				pmid.x = double(p1.x+p2.x)/2;
				pmid.y = double(p1.y+p2.y)/2;	
				double deep = Norm(pmid,p3);
				if(deep>2+double(Edgeline.data_ori[i].size())/100.0)//凸度
				{
					double ratio =0;
					if(Norm(p1,p2)>5)//一致性筛选
					{
						if(p1.x!=p2.x)
						{
							double k = double(p1.y-p2.y)/double(p1.x-p2.x);
							double b = p1.y - k*p1.x;
							ratio = PointDistribution( Edgeline.data_ori[i],k,b );
						}
						else
						{
							ratio = NANDistribution( Edgeline.data_ori[i],p1.x );
						}
					}
					if(ratio<0.1)
					{
						Segments.data_ori.push_back(Edgeline.data_ori[i]); 
						Segments.data_gauss.push_back(Edgeline.data_gauss[i]); 
						Segments.K.push_back(Edgeline.K[i]); 
						Segments.data_num.push_back(Edgeline.data_num[i]); 
						Segments.mean_k.push_back(Edgeline.mean_k[i]); 
						Segments.curve_deep.push_back(deep);
					}
				}
			}
		}
		else
		{
			int startloc = 0;
			index.push_back(Edgeline.data_num[i]-1);
			for(int j=0;j<index.size();j++)
			{
				int endloc = index[j];
				int Ni = endloc-startloc-2*n+1;
				if(Ni>TL)//长度筛选
				{
					vector<double> Ki;
					Ki.assign(Edgeline.K[i].begin()+startloc+n, Edgeline.K[i].begin()+endloc-n+1);
				    double mean_Ki=Mean(Ki);//平均曲率
			        if(mean_Ki>Tline)//非直线筛选
					{
						vector<Point> ori_data;
						ori_data.assign(Edgeline.data_ori[i].begin()+startloc+n, Edgeline.data_ori[i].begin()+endloc-n+1);
						Point p1 = ori_data[0];
						Point p2 = ori_data[ori_data.size()-1];
						Point p3 = ori_data[round(double(ori_data.size())/2.0)-1];
						Point2f pmid;
						pmid.x = double(p1.x+p2.x)/2;
						pmid.y = double(p1.y+p2.y)/2;	
						double deep = Norm(pmid,p3);
						if(deep>2+double(Ni)/100.0)//凸度
						{
							double ratio =0;
							if(Norm(p1,p2)>5)//一致性筛选
							{
								if(p1.x!=p2.x)
								{
									double k = double(p1.y-p2.y)/double(p1.x-p2.x);
									double b = p1.y - k*p1.x;
									ratio = PointDistribution( ori_data,k,b );
								}
								else
								{
									ratio = NANDistribution( ori_data,p1.x );
								}
							}
							if(ratio<0.1)
							{
								vector<Point2f> gauss_data;
								gauss_data.assign(Edgeline.data_gauss[i].begin()+startloc+n, Edgeline.data_gauss[i].begin()+endloc-n+1);

								Segments.data_ori.push_back(ori_data); 
								Segments.data_gauss.push_back(gauss_data); 
								Segments.K.push_back(Ki); 
								Segments.mean_k.push_back(mean_Ki); 
								Segments.data_num.push_back(Ni); 
								Segments.curve_deep.push_back(deep); 
							}
						}
					}
				}
				startloc = endloc+1;
			}
		}

	}
	return Segments;
}

//升序排序
vector<int> sort_asc(vector<double> &v)
{
	vector<int> idx(v.size());
	iota(idx.begin(), idx.end(), 0);
	sort(idx.begin(), idx.end(),
		[&v](size_t i1, size_t i2) {return v[i1] < v[i2]; });
	return idx;
}
//降序排序
vector<int> sort_des(vector<double> &v)
{
	vector<int> idx(v.size());
	iota(idx.begin(), idx.end(), 0);
	sort(idx.begin(), idx.end(),
		[&v](size_t i1, size_t i2) {return v[i1] > v[i2]; });
	return idx;
}
vector<int> sort_Intdes(vector<int> &v)
{
	vector<int> idx(v.size());
	iota(idx.begin(), idx.end(), 0);
	sort(idx.begin(), idx.end(),
		[&v](size_t i1, size_t i2) {return v[i1] > v[i2]; });
	return idx;
}

//归一化Point2f
Point2f Norm2f(Point2f V)
{
	double s = pow(double(pow(V.x,2)+pow(V.y,2)),0.5);
	V.x = V.x/s;
	V.y = V.y/s;
	return V;
}
//计算余弦
double GetCos(Point2f v1,Point2f v2)
{
	double t;
	t = v1.x*v2.x+v1.y*v2.y;
	return t;
}
//计算法向量
Point2f NormalVector(Point2f MidEnd,Point2f Midpoint,Point p1,Point p2)
{
	Point2f V1= MidEnd - Midpoint;
	V1 = Norm2f(V1);
	Point2f VLine = p1-p2;
	VLine = Norm2f(VLine);
	Point2f VL1;
	VL1.x = -VLine.y;VL1.y = VLine.x;
	Point2f VL2;
	VL2.x = VLine.y;VL2.y = -VLine.x;
	double A1 = acos(GetCos(VL1,V1));
	double A2 = acos(GetCos(VL2,V1));
	if(abs(A1)<abs(A2))
	{
		V1 = VL1;
	}
	else
	{
		V1 = VL2;
	}
	return V1;
}
//曲线参数计算
void CurveParameter(vector<vector<Point>> Data,vector<Point2f> &Loc,vector<Point2f> &Vcur)
{
	int num = Data.size();
	for(int i=0;i<num;i++)
	{
		int N = Data[i].size();
		Point p1 = Data[i][0];//端点
		Point p2 = Data[i][N-1];
		Point2f Midpoint;
		Midpoint.x = double(Data[i][floor(double(N)/2.0)-2].x+Data[i][floor(double(N)/2.0)-1].x+Data[i][floor(double(N)/2.0)].x)/3;
		Midpoint.y = double(Data[i][floor(double(N)/2.0)-2].y+Data[i][floor(double(N)/2.0)-1].y+Data[i][floor(double(N)/2.0)].y)/3;
		Point2f MidEnd = p1+p2;
		MidEnd.x = MidEnd.x/2;
		MidEnd.y = MidEnd.y/2;

		Point2f Vi;
		Vi = NormalVector(MidEnd,Midpoint,p1,p2);
		Point2f Loci;
		Loci = Midpoint;
		Vcur.push_back(Vi);
		Loc.push_back(Loci);
	}

}

//匹配
vector<vector<int>> CurveMatch(vector<Point2f> Loc,vector<Point2f> Vcur,int n)
{
	vector<vector<int>> Label(n,vector<int>(n));//定义二维数组label[][]，n行 n列;
	for(int i=0;i<n;i++)
	{
		Point2f L = Loc[i];
		Point2f V = Vcur[i];
		for(int j=0;j<n;j++)
		{
			if(i!=j)
			{
				Point2f Lj = Loc[j];
				Point2f Vj= Vcur[j];
				Point2f RelL = Lj-L;
				Point2f Vr= Norm2f(RelL);
				double A1 = acos(GetCos(Vr,V))*180/pi;
				double A2 = acos(GetCos(Vj,V))*180/pi;
				if(A1<90 && A2>20)
				{
					Label[i][j]=1;
				}
				else
				{
					Label[i][j]=0;
				}
			}
			else
			{
				Label[i][j]=1;
			}
		}
	}
	for(int i=0;i<n-1;i++)
	{
		for(int j=i+1;j<n;j++)
		{
			if(Label[i][j]!=Label[j][i])
			{
				Label[i][j]=0;
				Label[j][i]=0;
			}
		}
	}

	return Label;
}

//二次匹配，计算顺时针旋转角度
void MatchAngle(vector<vector<int>> &Label,vector<vector<double>> &Angle,vector<double> &RotateAngle,int n,vector<vector<Point2f>> GaussData,vector<vector<Point>> OriData,vector<Point2f> Loc,vector<int> number)
{
	//转置一下
	for(int ic=0;ic<Loc.size();ic++)
	{
		double a = Loc[ic].x;
		Loc[ic].x = Loc[ic].y;
		Loc[ic].y = a;
	}
	for(int i=0;i<GaussData.size();i++)
	{
		for(int j=0;j<GaussData[i].size();j++)
		{
			float a = GaussData[i][j].x;
			GaussData[i][j].x = GaussData[i][j].y;
			GaussData[i][j].y = a;

			int b = OriData[i][j].x;
			OriData[i][j].x = OriData[i][j].y;
			OriData[i][j].y = b;
		}
	}

	vector<vector<double>> Ve(n,vector<double>(2));//定义二维数组Ve[][]，curve.size()行 2列
	vector<vector<double>> P(n,vector<double>(4));//存储直线斜率截距[k1][b1][k2][b2]
	vector<Point2f> data;
	for (int i=0;i<n;i++)
	{
		data = GaussData[i];

		int si = data.size();
		int a1,a2;
		if(si<30)
		{a1=5;a2 = min(14,si-1);}
		else
		{a1=5;a2 = 14;}
		vector<Point2f> xy1(data.begin()+a1-1,data.begin()+a2);

		double ve_i[2];
		ve_i[0] = xy1[xy1.size()-1].y-xy1[0].y;
		ve_i[1] = xy1[xy1.size()-1].x-xy1[0].x;

		double ve[2];
		ve[0] = ve_i[0]/pow(pow(ve_i[0],2.0)+pow(ve_i[1],2.0),0.5);
		ve[1] = ve_i[1]/pow(pow(ve_i[0],2.0)+pow(ve_i[1],2.0),0.5);
		Ve[i][0] = ve[0];
		Ve[i][1] = ve[1];
		Vec4f para_line;//拟合方法采用最小二乘法para_line(向量+点)
		fitLine(xy1,para_line,CV_DIST_L2,0,0.01,0.01);
		P[i][0] = para_line[0]/para_line[1];
		P[i][1] = para_line[2]-para_line[0]/para_line[1]*para_line[3];

		vector<Point2f> xy2(data.end()-a2,data.end()-a1+1);
		fitLine(xy2,para_line,CV_DIST_L2,0,0.01,0.01);
		P[i][2] = para_line[0]/para_line[1];
		P[i][3] = para_line[2]-para_line[0]/para_line[1]*para_line[3];
	}
	for (int i=0;i<n;i++)
	{
		double Loc_i[2];
		Loc_i[0] = Loc[number[i]].x;
		Loc_i[1] = Loc[number[i]].y;
		for(int j=0;j<n;j++)
		{
			if(Label[i][j]==1 && i!=j)
			{
				double k1 = P[i][0];
				double b1 = P[i][1];

				double k2 = P[i][2];
				double b2 = P[i][3];

				vector<Point> datao = OriData[j];
				double ratio1 = PointDistributionT( datao,k1,b1 );
				double ratio2 = PointDistributionT( datao,k2,b2 );
				if(ratio1+ratio2>0.06)//判断是否当前曲线与另一条曲线的切线是否相交`
				{
					Label[i][j] = 0;
					Label[j][i] = 0;
				}

				double Loc_j[2];
				Loc_j[0] = Loc[number[j]].x;//判断曲线对是否在切线的同一侧
				Loc_j[1] = Loc[number[j]].y;

				double Sign1=(Loc_i[0]-(k1*Loc_i[1]+b1))*(Loc_j[0]-(k1*Loc_j[1]+b1));
                double Sign2=(Loc_i[0]-(k2*Loc_i[1]+b2))*(Loc_j[0]-(k2*Loc_j[1]+b2));
				if(Sign1<0 || Sign2<0)
				{
					Label[i][j] = 0;
					Label[j][i] = 0;
				}
			}
		}
	}


	//切线角度
	vector<vector<double>> Vk(n,vector<double>(4));//存储
	vector<double> Rotate_Angle(n);//存储
	for(int i=0;i<n;i++)
	{
		double k1 = P[i][0];
		Point p1 = OriData[i][0];

		double k2 = P[i][2];
		Point p2 = OriData[i][OriData[i].size()-1];

		double v1[2];
		double v2[2];
		if(p1.x==p2.x)
		{
			if(p1.y>p2.y)
			{
				v1[0] = (k1/abs(k1))/pow((1+pow(k1,2.0)),0.5);
				v1[1] = abs(k1)/pow((1+pow(k1,2.0)),0.5);

				v2[0] = -(k2/abs(k2))/pow((1+pow(k2,2.0)),0.5);
				v2[1] = -abs(k2)/pow((1+pow(k2,2.0)),0.5);
			}
			else
			{
				v1[0] = -(k1/abs(k1))/pow((1+pow(k1,2.0)),0.5);
				v1[1] = -abs(k1)/pow((1+pow(k1,2.0)),0.5);

				v2[0] = (k2/abs(k2))/pow((1+pow(k2,2.0)),0.5);
				v2[1] = abs(k2)/pow((1+pow(k2,2.0)),0.5);
			}
		}
		else if(p1.x>p2.x)
		{
			double Vline[2] = {p1.y-p2.y,p1.x-p2.x};
			double VLine[2];
			VLine[0] = Vline[0]/pow(pow(Vline[0],2.0)+pow(Vline[1],2.0),0.5);
			VLine[1] = Vline[1]/pow(pow(Vline[0],2.0)+pow(Vline[1],2.0),0.5);

			double Vl1[2] = {1,-VLine[0]/VLine[1]};
			double VL1[2];
			VL1[0] = Vl1[0]/pow(pow(Vl1[0],2.0)+pow(Vl1[1],2.0),0.5);
			VL1[1] = Vl1[1]/pow(pow(Vl1[0],2.0)+pow(Vl1[1],2.0),0.5);

		    double Vl2[2] = {-1,VLine[0]/VLine[1]};
			double VL2[2];
			VL2[0] = Vl2[0]/pow(pow(Vl2[0],2.0)+pow(Vl2[1],2.0),0.5);
			VL2[1] = Vl2[1]/pow(pow(Vl2[0],2.0)+pow(Vl2[1],2.0),0.5);

			v1[0] = 1/pow((1+pow(k1,2.0)),0.5);
			v1[1] = k1/pow((1+pow(k1,2.0)),0.5);
			if(acos(VL2[0]*v1[0]+VL2[1]*v1[1])*180/pi>90)
			{
				v1[0] = -v1[0];
				v1[1] = -v1[1];
			}

			v2[0] = 1/pow((1+pow(k2,2.0)),0.5);
			v2[1] = k2/pow((1+pow(k2,2.0)),0.5);
			if(acos(VL1[0]*v2[0]+VL1[1]*v2[1])*180/pi>90)
			{
				v2[0] = -v2[0];
				v2[1] = -v2[1];
			}
		}
		else if(p1.x<p2.x)
		{
			double Vline[2] = {p1.y-p2.y,p1.x-p2.x};
			double VLine[2];
			VLine[0] = Vline[0]/pow(pow(Vline[0],2.0)+pow(Vline[1],2.0),0.5);
			VLine[1] = Vline[1]/pow(pow(Vline[0],2.0)+pow(Vline[1],2.0),0.5);

			double Vl1[2] = {1,-VLine[0]/VLine[1]};
			double VL1[2];
			VL1[0] = Vl1[0]/pow(pow(Vl1[0],2.0)+pow(Vl1[1],2.0),0.5);
			VL1[1] = Vl1[1]/pow(pow(Vl1[0],2.0)+pow(Vl1[1],2.0),0.5);

		    double Vl2[2] = {-1,VLine[0]/VLine[1]};
			double VL2[2];
			VL2[0] = Vl2[0]/pow(pow(Vl2[0],2.0)+pow(Vl2[1],2.0),0.5);
			VL2[1] = Vl2[1]/pow(pow(Vl2[0],2.0)+pow(Vl2[1],2.0),0.5);

			v1[0] = 1/pow((1+pow(k1,2.0)),0.5);
			v1[1] = k1/pow((1+pow(k1,2.0)),0.5);
			if(acos(VL1[0]*v1[0]+VL1[1]*v1[1])*180/pi>90)
			{
				v1[0] = -v1[0];
				v1[1] = -v1[1];
			}

			v2[0] = 1/pow((1+pow(k2,2.0)),0.5);
			v2[1] = k2/pow((1+pow(k2,2.0)),0.5);
			if(acos(VL2[0]*v2[0]+VL2[1]*v2[1])*180/pi>90)
			{
				v2[0] = -v2[0];
				v2[1] = -v2[1];
			}
		}
		Vk[i][0] = v1[0];//v1
		Vk[i][1] = v1[1];
		Vk[i][2] = v2[0];//v2
		Vk[i][3] = v2[1];

		double ve[2] = {Ve[i][0],Ve[i][1]};
		if(acos(ve[0]*v1[0]+ve[1]*v1[1])*180/pi>90)
		{
			double v0[2] = {v1[0],v1[1]};
			v1[0] = v2[0];
			v1[1] = v2[1];

			v2[0] = v0[0];
			v2[1] = v0[1];
		}

		double Vx[2] = {1,0};
		double angle = (v1[1]/abs(v1[1]))*acos(Vx[0]*v1[0]+Vx[1]*v1[1])*180/pi;
		if(angle<0)
		{Angle[i][0] = angle+360;}
		else
		{Angle[i][0] = angle;}

		angle =  (v2[1]/abs(v2[1]))*acos(Vx[0]*v2[0]+Vx[1]*v2[1])*180/pi;
		if(angle<0)
		{Angle[i][1] = angle+360;}
		else
		{Angle[i][1] = angle;}

		if(Angle[i][1]<Angle[i][0])
		{
			RotateAngle[i] = Angle[i][1]-Angle[i][0]+360;
		}
		else
		{
			RotateAngle[i] = Angle[i][1]-Angle[i][0];
		}

	}
}

//求所有子集-基于位运算的非递归方法
void Subsets(vector<vector<int>> &Set,vector<int> arr, int n)
{
	for (long i=0; i<(1<<n); ++i)
	{
		vector<int> set;
		long j = i;
		int idx = 0;
		while (j>0)
		{
			if (j&1)
			{
				set.push_back(arr[idx]);
			}
			j = j>>1;//必须赋值
			++idx;
		}
		Set.push_back(set);
	}
	Set.erase( Set.begin());
}


//
int Compare(vector<int> set1,vector<int> set2)
{
	int S=1;
	for(int s1=0;s1<set1.size();s1++)
	{
		int a = set1[s1];
		for(int s2=0;s2<set2.size();s2++)
		{
			if(a == set2[s2])
			{
				S=0;
				break;
			}
		}
	}
	return S;
}

// 仿照matlab，自适应求高低两个门限                                            
void _AdaptiveFindThreshold(CvMat *dx, CvMat *dy, float *low, float *high)   

{                                                                              

	CvSize size;                                                           

	IplImage *imge=0;                                                      

	int i,j;                                                               

	CvHistogram *hist;                                                     

	int hist_size = 255;                                                   

	float range_0[]={0,256};                                               

	float* ranges[] = { range_0 };                                         

	float PercentOfPixelsNotEdges = 0.8;                                  

	size = cvGetSize(dx);                                                  

	imge = cvCreateImage(size, IPL_DEPTH_32F, 1);                          

	// 计算边缘的强度, 并存于图像中                                        

	float maxv = 0;                                                        

	for(i = 0; i < size.height; i++ )                                      

	{                                                                      

		const short* _dx = (short*)(dx->data.ptr + dx->step*i);        

		const short* _dy = (short*)(dy->data.ptr + dy->step*i);        

		float* _image = (float *)(imge->imageData + imge->widthStep*i);

		for(j = 0; j < size.width; j++)                                

		{                                                              

			_image[j] = (float)(abs(_dx[j]) + abs(_dy[j]));        

			maxv = maxv < _image[j] ? _image[j]: maxv;             

	                                                                       

		}                                                              

	}                                                                      

	if(maxv == 0){                                                         

		*high = 0;                                                     

		*low = 0;                                                      

		cvReleaseImage( &imge );                                       

		return;                                                        

	}                                                                      
                                                                              
	// 计算直方图                                                          

	range_0[1] = maxv;                                                     

	hist_size = (int)(hist_size > maxv ? maxv:hist_size);                  

	hist = cvCreateHist(1, &hist_size, CV_HIST_ARRAY, ranges, 1);          

	cvCalcHist( &imge, hist, 0, NULL );                                    

	int total = (int)(size.height * size.width * PercentOfPixelsNotEdges); 

	float sum=0;                                                           

	int icount = hist->mat.dim[0].size;                                    

                                                                               

	float *h = (float*)cvPtr1D( hist->bins, 0 );                           

	for(i = 0; i < icount; i++)                                            

	{                                                                      

		sum += h[i];                                                   

		if( sum > total )                                              

			break;                                                 

	}                                                                      

	// 计算高低门限                                                        

	*high = (i+1) * maxv / hist_size ;                                     

	*low = *high * 0.4;                                                    

	cvReleaseImage( &imge );                                               

	cvReleaseHist(&hist);                                                  
} 
void AdaptiveFindThreshold(Mat src, float *low, float *high, int aperture_size=3)
{                                                                                                          
	const int cn = src.channels();                                         

	cv::Mat dx(src.rows, src.cols, CV_16SC(cn));                           

	cv::Mat dy(src.rows, src.cols, CV_16SC(cn));                                                                                                         

	cv::Sobel(src, dx, CV_16S, 1, 0, aperture_size, 1, 0);

	cv::Sobel(src, dy, CV_16S, 0, 1, aperture_size, 1, 0);
                                                                             
	//CvMat _dx = cvMat(dx), _dy = cvMat(dy);                                              
	CvMat _dx = dx, _dy = dy;
	_AdaptiveFindThreshold(&_dx, &_dy, low, high);                         
                                                                            
}                                                                                                                                                  
         
//绘制曲线
void plot_curves(Data_Parameters CurveSelected,Mat srcImage,string WindowName)
{
	for(int i=0;i<CurveSelected.data_num.size();i++)
	{
		for(int j=0;j<CurveSelected.data_num[i];j++)
		{
			circle(srcImage, CurveSelected.data_ori[i][j],0.5,Scalar(0,0,255),-1); //第五个参数设为-1，表明这是个实点。
		}
	}
	imshow(WindowName, srcImage);
}

struct timespec time_start_read = {0, 0}; 
struct timespec time_start_proc = {0, 0};
struct timespec time_end_read = {0, 0};
struct timespec time_end_proc = {0, 0};

struct timespec time_start_breakpoint = {0, 0};
struct timespec time_end_breakpoint = {0, 0};

int segmentCount = 0;

#define timeStamp_ms_readImage (unsigned long long)(time_end_read.tv_sec*1000+time_end_read.tv_nsec/1000000) - (unsigned long long)(time_start_read.tv_sec*1000+time_start_read.tv_nsec/1000000)
#define timeStamp_ms_processImage (unsigned long long)(time_end_proc.tv_sec*1000+time_end_proc.tv_nsec/1000000) - (unsigned long long)(time_start_proc.tv_sec*1000+time_start_proc.tv_nsec/1000000)
#define timeStamp_ms_segment (unsigned long long)(time_end_breakpoint.tv_sec*1000+time_end_breakpoint.tv_nsec/1000000) - (unsigned long long)(time_start_breakpoint.tv_sec*1000+time_start_breakpoint.tv_nsec/1000000)

#ifdef myTimer
void startMyTimer()
{
	clock_gettime(CLOCK_REALTIME, &time_start_breakpoint);
}

void endMyTimer()
{
	unsigned long long time_segment = 0;
	++ segmentCount;
	clock_gettime(CLOCK_REALTIME, &time_end_breakpoint);
	time_segment = timeStamp_ms_segment;
	printf("第 %d 段的运行时间是 %llu ms\n\r", segmentCount, time_segment);
}
#endif

Mat myCutPicture(Mat dstImage)   //将dstImage中的大片无用背景剪裁并保存回GrayImage中
{
	int width, height;
	int maxRow, maxCol, minRow, minCol;
	Mat GrayImage;

	height = dstImage.rows;
	width = dstImage.cols;
	maxRow = 0;
	maxCol = 0;
	minRow = height;
	minCol = width;
#ifdef parametersLog
	printf("height = %d width = %d\n\r", height, width);
#endif

	for(int i = 0; i < height; ++ i) 
	{
		unsigned char* data = dstImage.ptr<unsigned char>(i);
		for(int j = 0; j < width; ++ j)
		{
			if(data[j]) //根据阈值来筛选裁剪区域   	0-表示黑色  255-表示白色
			{
				maxRow = max(maxRow, i);
				minRow = min(minRow, i);
				maxCol = max(maxCol, j);
				minCol = min(minCol, j);
			}
		}
	}

	//向外增长5个像素
	maxRow = min(maxRow, height);
	maxCol = min(maxCol, width);
	minRow = max(minRow-5, 0);
	minCol = max(minCol-5, 0);
	
	GrayImage = dstImage(Range(minRow, maxRow), Range(minCol, maxCol));
	return GrayImage;
}

void EllipseDetection(string input_filename)//主函数
{
	segmentCount = 0;
	unsigned long long time_readpic = 0;
	unsigned long long time_processpic = 0;
	
	clock_gettime(CLOCK_REALTIME, &time_start_read);

	static int picOrder = 0;
	++ picOrder;
	//读入图像
	Mat srcImage,dstImage,GrayImage,grayImage;
	srcImage = imread(input_filename);
	if (srcImage.type() == CV_8UC3)
	{
		cvtColor(srcImage, dstImage, COLOR_BGR2GRAY);
		GrayImage = myCutPicture(dstImage);
	}
	else
	{
		GrayImage = myCutPicture(srcImage);
	}
	//imwrite("GrayImage.bmp", GrayImage);
#ifdef parametersLog
	printf("GrayImage.rows=%d GrayImage.cols=%d\n\r", GrayImage.rows, GrayImage.cols);
#endif

	clock_gettime(CLOCK_REALTIME, &time_end_read);
	time_readpic = timeStamp_ms_readImage;

#ifdef log
		printf("File : %s Line : %d\n\r", __FILE__, __LINE__);
#endif

	//输入参数
	float sigma_pre = 3;//预处理sigma
	float TL_input = 4;//TL
	float Td_input = 1.5;//Td
	sigma = 3;//高斯演化sigma
	RD_input = 0.02;//rD-0.02
    Rd_input = 0.95;//rd-0.95
	TScore_input = 1.9;//Tscore-1.9
	int control_num = 25;//控制曲线数量

	clock_gettime(CLOCK_REALTIME, &time_start_proc);
//segment 1
#ifdef myTimer
	startMyTimer();
#endif
	GaussianBlur(GrayImage, GrayImage, Size(sigma_pre, sigma_pre),0,0);//高斯模糊
#ifdef myTimer
	endMyTimer();
#endif

#ifdef log
		printf("File : %s Line : %d\n\r", __FILE__, __LINE__);
#endif

	float low = 0.0, high = 0.0;
//segment2
#ifdef myTimer
		startMyTimer();
#endif
#if 0
	static bool thresholdFlag = false;
	static float lowValue, highValue;
	if(!thresholdFlag) {
		AdaptiveFindThreshold(GrayImage, &lowValue, &highValue);//待优化
		thresholdFlag = true;
		//printf("threshold: low:%.2lf high:%.2lf\n\r", lowValue, highValue);
	}	
	low = lowValue;
	high = highValue;
#endif
#ifdef parametersLog
		printf("第 %d 张图片的阈值是：low:%f high:%f\n\r", picOrder, low, high);
#endif
#ifdef myTimer
	endMyTimer();
#endif	
//segment3
#ifdef myTimer
	startMyTimer();
#endif
	Canny(GrayImage, grayImage, 50, 150);
	float TL = float(grayImage.rows+grayImage.cols)/TL_input;//长度阈值
#ifdef parametersLog
	printf("TL = %.2lf\n\r", TL);
#endif
	//连续边缘提取
	vector<vector<Point>> curve;//连续边缘
	int cur_num = 0;//边缘数量
#ifdef myTimer
	endMyTimer();
#endif

//segment4
#ifdef myTimer
	startMyTimer();
#endif

	curve = extract_curve(grayImage,TL,&cur_num);

#ifdef log
		printf("File : %s Line : %d\n\r", __FILE__, __LINE__);
#endif

#ifdef myTimer
	endMyTimer();
#endif

	//曲线分类，计算曲率等参数
	Data_Parameters Edgeline;
	Data_Parameters Edgeloop;
//segment5
#ifdef myTimer
	startMyTimer();
#endif

	Curve_Classify(curve,cur_num,&Edgeline,&Edgeloop);

	int numloopedge = Edgeloop.mean_k.size();//闭合曲线数量
	int numlineedge = Edgeline.mean_k.size();//非闭合曲线数量

#ifdef log
	printf("File : %s Line : %d\n\r", __FILE__, __LINE__);
#endif

	//整体参数设置
	float det_inner = Td_input;
	vector<vector<double>> f_5D;//每个vector5个椭圆参数
	int num_ellipse = 0;
#ifdef myTimer
	endMyTimer();
#endif

//segment6
#ifdef myTimer
	startMyTimer();
#endif

	//第一次拟合（闭合曲线）
	vector<double> ellipse_5D(5);
	double pe[6];
	double Det;
	if(numloopedge>0)
	{
		for(int i=0;i<numloopedge;i++)
		{
			double k_loop = Edgeloop.mean_k[i];
			int S=0;
			if( k_loop<0.15 && k_loop>0.01 && Edgeloop.data_num[i]>4*TL )
			{
				vector<Point> data = Edgeloop.data_ori[i];
				FIT_Ellipses(data,ellipse_5D,pe,&Det);

				if(Det<0 && ellipse_5D[0]/ellipse_5D[1]<3 && ellipse_5D[0]>TL/2)
				{
					FitAndSelect(ellipse_5D,f_5D,&num_ellipse,data,pe,Edgeloop.data_num[i],det_inner,&S);
				}
			}
			if(S==0)
			{
				Edgeline.data_ori.push_back(Edgeloop.data_ori[i]);
				Edgeline.data_gauss.push_back(Edgeloop.data_gauss[i]);
				Edgeline.data_num.push_back(Edgeloop.data_num[i]);
				Edgeline.K.push_back(Edgeloop.K[i]);
				Edgeline.mean_k.push_back(k_loop);
			}
		}
	}

#ifdef log
		printf("File : %s Line : %d\n\r", __FILE__, __LINE__);
#endif
#ifdef myTimer
	endMyTimer();
#endif

//segment7
#ifdef myTimer
		startMyTimer();
#endif

	//断开曲线
	cur_num = numlineedge+numloopedge-num_ellipse;

	vector<vector<int>> corners_index(cur_num);//角点位置索引
	Data_Parameters Segments;
	double Tline = 0.014;//直线阈值  原来是0.006
	if(cur_num>0)
	{
		corners_index = CornersDetection(Edgeline.K,Edgeline.data_num,cur_num);
		Segments = BreakCurve(Edgeline,corners_index,cur_num,TL,Tline);//长度、曲率、凸度、有序性筛选
	}

#ifdef log
		printf("File : %s Line : %d\n\r", __FILE__, __LINE__);
#endif
#ifdef myTimer
	endMyTimer();
#endif

//segment8
#ifdef myTimer
		startMyTimer();
#endif

	//椭圆拟合2-闭合曲线筛选
	Data_Parameters CurveSelected;//Cur_5
	if(Segments.data_num.size()>0)
	{
		for(int i=0;i<Segments.data_num.size();i++)
		{
			vector<Point> data = Segments.data_ori[i];
			Point p1 = data[0];
			Point p2 = data[data.size()-1];
			int S = 0;
			if(Norm(p1,p2)<0.45*Segments.data_num[i] && Segments.curve_deep[i]>TL )
			{
				FIT_Ellipses(data,ellipse_5D,pe,&Det);

				if(Det<0 && ellipse_5D[0]/ellipse_5D[1]<3 && ellipse_5D[0]>TL/2)
				{
					FitAndSelect(ellipse_5D,f_5D,&num_ellipse,data,pe,Segments.data_num[i],det_inner,&S);
				}
			}
			if(S==0)
			{
				CurveSelected.data_ori.push_back(Segments.data_ori[i]);
				CurveSelected.data_gauss.push_back(Segments.data_gauss[i]);
				CurveSelected.data_num.push_back(Segments.data_num[i]);
				CurveSelected.K.push_back(Segments.K[i]);
				CurveSelected.mean_k.push_back(Segments.mean_k[i]);
				CurveSelected.curve_deep.push_back(Segments.curve_deep[i]);
			}
		}
	}

#ifdef log
		printf("File : %s Line : %d\n\r", __FILE__, __LINE__);
#endif
#ifdef myTimer
	endMyTimer();
#endif

//segment9
#ifdef myTimer
		startMyTimer();
#endif

	//曲线匹配
	//double t = (double)getTickCount();//计时开始
	int num_CurveSelected = CurveSelected.data_num.size();
	vector<Point2f> Loc;//位置
	vector<Point2f> Vcur;//向量
	vector<vector<int>> Label(num_CurveSelected,vector<int>(num_CurveSelected));//定义二维数组label[][]，n行 m列
	vector<vector<int>> AllSet;//所有待拟合集合
	if(num_CurveSelected>0)
	{
		//曲线数量控制
		if(num_CurveSelected>control_num)
		{
			vector<int> index_deep;
			index_deep = sort_asc(CurveSelected.curve_deep);//升序排序，返回索引位置des

			vector<int> index_del;
			index_del.assign(index_deep.begin(),index_deep.begin()+num_CurveSelected-control_num);
			sort(index_del.begin(),index_del.end(),greater<int>());//降序排序，返回索引位置des

			for(int i=0;i<num_CurveSelected-control_num;i++)
			{
				CurveSelected.data_ori.erase( CurveSelected.data_ori.begin()+index_del[i]);//删除容器，删除速度快于复制
				CurveSelected.data_gauss.erase( CurveSelected.data_gauss.begin()+index_del[i]);
				CurveSelected.data_num.erase( CurveSelected.data_num.begin()+index_del[i]);
				CurveSelected.curve_deep.erase( CurveSelected.curve_deep.begin()+index_del[i]);
				CurveSelected.K.erase( CurveSelected.K.begin()+index_del[i]);
				CurveSelected.mean_k.erase( CurveSelected.mean_k.begin()+index_del[i]);
			}
		}
		num_CurveSelected = CurveSelected.data_num.size();

		//Mat image_CurveSelected = srcImage.clone();
		//plot_curves(CurveSelected,image_CurveSelected,"CurveSelected");//****绘制曲线****//

		//匹配1：基于位置方向
		CurveParameter(CurveSelected.data_ori,Loc,Vcur);
		Label = CurveMatch(Loc,Vcur,num_CurveSelected);//分布问题
		//未匹配的拟合
		vector<int> index_unmatched;
		vector<int> number;
		for(int i=0;i<num_CurveSelected;i++)
		{
			int n_match=accumulate(Label[i].begin(),Label[i].end(),0);//求和
			if(n_match==1)
			{
				if( CurveSelected.curve_deep[i]>10)
				{
					int S=0;
					FIT_Ellipses(CurveSelected.data_ori[i],ellipse_5D,pe,&Det);
					double ratio_perimeter = CurveSelected.data_num[i]/(pi*(1.5*(ellipse_5D[0]+ellipse_5D[1])-pow((ellipse_5D[0]*ellipse_5D[1]),0.5)));
					if(Det<0 && ellipse_5D[0]/ellipse_5D[1]<3 && ((ratio_perimeter>0.5 && CurveSelected.curve_deep[i]>TL/2)||(ratio_perimeter>0.3 && CurveSelected.curve_deep[i]>TL)))
					{
						FitAndSelect(ellipse_5D,f_5D,&num_ellipse,CurveSelected.data_ori[i],pe,CurveSelected.data_num[i],det_inner,&S);
					}
				}
				//移除
				index_unmatched.push_back(i);
			}
			else if(n_match>1)
			{
				number.push_back(i);
			}
		}
		//移除
		if(index_unmatched.size()>0)//表明有未匹配的曲线，移除
		{
			for(int i=index_unmatched.size()-1;i>=0;i--)
			{
				CurveSelected.data_ori.erase( CurveSelected.data_ori.begin()+index_unmatched[i]);//删除容器，删除速度快于复制
				CurveSelected.data_gauss.erase( CurveSelected.data_gauss.begin()+index_unmatched[i]);
				CurveSelected.data_num.erase( CurveSelected.data_num.begin()+index_unmatched[i]);
				CurveSelected.curve_deep.erase( CurveSelected.curve_deep.begin()+index_unmatched[i]);
				CurveSelected.K.erase( CurveSelected.K.begin()+index_unmatched[i]);
				CurveSelected.mean_k.erase( CurveSelected.mean_k.begin()+index_unmatched[i]);
			}
		}
		//二次匹配
		num_CurveSelected = CurveSelected.data_num.size();
		vector<vector<int>> LabelTwo(num_CurveSelected,vector<int>(num_CurveSelected));//定义二维数组label
		if(num_CurveSelected>0)
		{
		    for(int i=0;i<num_CurveSelected;i++)
			{
				int a = number[i];
				for(int j=0;j<num_CurveSelected;j++)
				{
					int b = number[j];
					LabelTwo[i][j] = Label[a][b];
				}
			}
			//二次匹配
			CurveSelected.angle.resize(num_CurveSelected,vector<double>(2));
			CurveSelected.ratation_angle.resize(num_CurveSelected);

			for(int ic=0;ic<num_CurveSelected;ic++)
			{
				CurveSelected.data_gauss[ic].clear();
				vector<Point2f> gaussdata = GaussData(CurveSelected.data_ori[ic],3.0);
				CurveSelected.data_gauss[ic] = gaussdata;
			}
			MatchAngle(LabelTwo,CurveSelected.angle,CurveSelected.ratation_angle,num_CurveSelected,CurveSelected.data_gauss,CurveSelected.data_ori,Loc,number);
			//落单曲线拟合
			vector<int> index_alone;
			vector<int> numberpair;
			for(int i=0;i<num_CurveSelected;i++)
			{
				int n_match=accumulate(LabelTwo[i].begin(),LabelTwo[i].end(),0);//求和
				if(n_match==1)
				{
					index_alone.push_back(i);
					if(CurveSelected.ratation_angle[i]>120)
					{
						int S=0;
						FIT_Ellipses(CurveSelected.data_ori[i],ellipse_5D,pe,&Det);

						double ratio_perimeter = CurveSelected.data_num[i]/(pi*(1.5*(ellipse_5D[0]+ellipse_5D[1])-pow((ellipse_5D[0]*ellipse_5D[1]),0.5)));
						if(Det<0 && ellipse_5D[0]/ellipse_5D[1]<3 && ((ratio_perimeter>0.5 && CurveSelected.curve_deep[i]>TL/2)||(ratio_perimeter>0.3 && CurveSelected.curve_deep[i]>TL)))
						{
							FitAndSelect(ellipse_5D,f_5D,&num_ellipse,CurveSelected.data_ori[i],pe,CurveSelected.data_num[i],det_inner,&S);
						}
					}
				}
				else
				{
					numberpair.push_back(i);
				}
			}
			//移除
			if(index_alone.size()>0)//表明有未匹配的曲线，移除
			{
				for(int i=index_alone.size()-1;i>=0;i--)
				{
					CurveSelected.data_ori.erase( CurveSelected.data_ori.begin()+index_alone[i]);//删除容器，删除速度快于复制
					CurveSelected.data_gauss.erase( CurveSelected.data_gauss.begin()+index_alone[i]);
					CurveSelected.data_num.erase( CurveSelected.data_num.begin()+index_alone[i]);
					CurveSelected.curve_deep.erase( CurveSelected.curve_deep.begin()+index_alone[i]);
					CurveSelected.K.erase( CurveSelected.K.begin()+index_alone[i]);
					CurveSelected.mean_k.erase( CurveSelected.mean_k.begin()+index_alone[i]);
					CurveSelected.angle.erase( CurveSelected.angle.begin()+index_alone[i]);
					CurveSelected.ratation_angle.erase( CurveSelected.ratation_angle.begin()+index_alone[i]);
				}
			}
			//LabelPair
			num_CurveSelected = CurveSelected.data_num.size();
			vector<vector<int>> LabelPair(num_CurveSelected,vector<int>(num_CurveSelected));
			vector<vector<int>> Elements(num_CurveSelected);
			if(num_CurveSelected>0)
			{
				for(int i=0;i<num_CurveSelected;i++)
				{
					for(int j=0;j<num_CurveSelected;j++)
					{
						if(i<=j)
						{
							int a = numberpair[i];
							int b = numberpair[j];
							LabelPair[i][j] = LabelTwo[a][b];
							if(LabelPair[i][j]==1 && i!=j)
							{
								Elements[i].push_back(j);
							}
						}
						else
						{
							LabelPair[i][j]=0;
						}
					}
				}
				//匹配曲线分组
				for(int i=0;i<num_CurveSelected;i++)
				{
					if(Elements[i].size()>0)
					{
						vector<int> set = Elements[i];
						int s = set.size();
						if(s==1)
						{
							set.insert(set.begin(),i);
							AllSet.push_back(set);
						}
						else
						{
							vector<vector<int>> set_group;
							Subsets(set_group,set, set.size());
							for(int j=0;j<set_group.size();j++)
							{
								if(set_group[j].size()>1)
								{
									int S=1;
									for(int ii=0;ii<set_group[j].size()-1;ii++)
									{
										for(int jj=ii+1;jj<set_group[j].size();jj++)
										{
											if(LabelPair[set_group[j][ii]][set_group[j][jj]]==0)
											{
												S=0;
												break;
											}
										}
									}
									if(S==1)
									{
										set_group[j].insert(set_group[j].begin(),i);
										AllSet.push_back(set_group[j]);
									}
								}
								else
								{
									set_group[j].insert(set_group[j].begin(),i);
							        AllSet.push_back(set_group[j]);
								}
							}
						}
					}
				}
			}
		}

	}

#ifdef log
		printf("File : %s Line : %d\n\r", __FILE__, __LINE__);
#endif
#ifdef myTimer
	endMyTimer();
#endif

//segment10
#ifdef myTimer
		startMyTimer();
#endif

	//拟合所有组合
	int Nset = AllSet.size();
	vector<double> Score;//候选椭圆得分
	vector<vector<double>> candidates;//候选椭圆集合的几何参数
	vector<vector<int>> group;
	vector<int> numgroup;
	if(Nset>0)
	{
		for(int i=0;i<Nset;i++)
		{
			vector<int> group_curve = AllSet[i];
			int n = group_curve.size();

			vector<Point> data;//组合的数据
			vector<vector<double>> angle_set(2,vector<double>(n));//存储角度,2行n列
			int index=0;
			for(int j=0;j<n;j++)//数据导入data
			{
				data.insert(data.begin()+index, CurveSelected.data_ori[group_curve[j]].begin(), CurveSelected.data_ori[group_curve[j]].end());
			    index = CurveSelected.data_ori[group_curve[j]].size();
				angle_set[0][j] = CurveSelected.angle[group_curve[j]][0];
				angle_set[1][j] = CurveSelected.angle[group_curve[j]][1];
			}

			vector<int> order = sort_asc(angle_set[0]);//升序
			vector<vector<double>> Angle_set(n,vector<double>(2));//存储角度,n行2列
			for(int j=0;j<n;j++)
			{
				Angle_set[j][0] =  angle_set[0][order[j]];
				Angle_set[j][1] =  angle_set[1][order[j]];
			}
			double Tangle=360;
			for(int j=0;j<n;j++)
			{
				double ang1 = Angle_set[j][0];
				double ang2 = Angle_set[j-1+(1-Sign_double(j))*n][1];
				double coverage;
				if(ang2>ang1)
					coverage = ang2-ang1;
				else
					coverage = ang2+360-ang1;
				if(coverage<Tangle)
					Tangle = coverage;
			}

			if(Tangle>150)
			{
				FIT_Ellipses(data,ellipse_5D,pe,&Det);

				if(Det<0)
				{
					double ratio_perimeter = data.size()/(pi*(1.5*(ellipse_5D[0]+ellipse_5D[1])-pow((ellipse_5D[0]*ellipse_5D[1]),0.5)));
				    if(((ratio_perimeter>0.3 || Tangle>240) && ellipse_5D[0]/ellipse_5D[1]<3) || (ratio_perimeter>0.9))
					{
						Point2f center;
						center.x = ellipse_5D[2];
						center.y = ellipse_5D[3];
						double *error_data;
						double ratio_inner,ratio_H;
						error_data = DetPix(data,center,pe,data.size(),det_inner,&ratio_inner,&ratio_H);
						double A = ratio_inner+ratio_perimeter+Tangle/360;
						if(A>TScore_input && ratio_H<RD_input)
						{
							Score.push_back(A);
							candidates.push_back(ellipse_5D);
							group.push_back(group_curve);
							numgroup.push_back(n);
						}
					}
				}
			}
		}
	}

#ifdef log
		printf("File : %s Line : %d\n\r", __FILE__, __LINE__);
#endif
#ifdef myTimer
	endMyTimer();
#endif

//segment11
#ifdef myTimer
		startMyTimer();
#endif

	//筛选
	CandidatesPara CandidatesData;
	int n = Score.size();
	if(n>0)
	{
		vector<int> indexScore =  sort_des(Score);//得分降序
		for(int i=0;i<n;i++)
		{
			CandidatesData.Score.push_back(Score[indexScore[i]]);
			CandidatesData.Group.push_back(group[indexScore[i]]);
			CandidatesData.ellipses.push_back(candidates[indexScore[i]]);
			CandidatesData.Num.push_back(numgroup[indexScore[i]]);
		}

		int max_num = *max_element(CandidatesData.Num.begin(), CandidatesData.Num.end());//最大元素数量
		int min_num = *min_element(CandidatesData.Num.begin(), CandidatesData.Num.end());//最小元素数量

		vector<vector<int>> index_num(max_num-min_num+1);
		for(int i=0;i<n;i++)
		{
			index_num[CandidatesData.Num[i]-min_num].push_back(i);
		}

		int n_delete = 0;
		for(int i=0;i<index_num.size();i++ )
		{
			vector<int> indexnum_i = index_num[i];
			if(indexnum_i.size()>0)
			{
				for(int j=0;j<indexnum_i.size()-1;j++)
				{
					vector<int> set1 = CandidatesData.Group[indexnum_i[j]];
					if(set1.size()>0)
					{
						for(int k=j+1;k<indexnum_i.size();k++)
						{
							vector<int> set2 = CandidatesData.Group[indexnum_i[k]];
							int S = Compare(set1,set2);
							if(S==0)
							{
								n_delete++;
								CandidatesData.Group[indexnum_i[k]].clear();
								CandidatesData.Num[indexnum_i[k]]=0;
							}
						}
					}
				}
			}
		}

		int n_remain = n-n_delete;
		n_delete=0;
		vector<int> index_Num = sort_Intdes(CandidatesData.Num);//降序
		for(int i=0;i<n_remain-1;i++)//二次筛选
		{
			vector<int> set1 = CandidatesData.Group[index_Num[i]];
			for(int j=i+1;j<n_remain;j++)
			{
				vector<int> set2 = CandidatesData.Group[index_Num[j]];
				if(set1.size()!=set2.size())
				{
					int S = Compare(set1,set2);
					if(S==0)
					{
						n_delete++;
						CandidatesData.Group[index_Num[j]].clear();
						CandidatesData.Num[index_Num[j]]=0;
					}
				}
			}
		}

		n_remain = n_remain-n_delete;
		index_Num = sort_Intdes(CandidatesData.Num);//降序
		for(int i=0;i<n_remain;i++)
		{
			f_5D.push_back(CandidatesData.ellipses[index_Num[i]]);
		}
	}

	int NumEllipses = f_5D.size();

#ifdef log
		printf("File : %s Line : %d\n\r", __FILE__, __LINE__);
#endif
#ifdef myTimer
	endMyTimer();
#endif

//segment12
#ifdef myTimer
		startMyTimer();
#endif

	//绘制椭圆
	for(int i=0;i<NumEllipses;i++)
	{
		ellipse(GrayImage,Point(f_5D[i][2],f_5D[i][3]),Size(f_5D[i][0],f_5D[i][1]),f_5D[i][4]*180/pi,0,360,Scalar(255,255,255),2,8,0);
	}
#ifdef myTimer
		endMyTimer();
#endif

	clock_gettime(CLOCK_REALTIME, &time_end_proc);
	time_processpic = timeStamp_ms_processImage;
	averageProcessTime = averageProcessTime + (double)time_processpic;
	//printf("第 %d 幅图像的读取时间：%llu ms，处理时间: %llu ms\n\r", picOrder, time_readpic, time_processpic);	

#ifdef log
	printf("File : %s Line : %d\n\r", __FILE__, __LINE__);
#endif
	string outputFilename;
	outputFilename = "/home/HwHiAiUser/output/" + to_string(picOrder) + ".bmp";
	imwrite(outputFilename, GrayImage);
	//imshow("椭圆", srcImage);
	//waitKey(1);
}
int main()
{	
	printf("start\n");
	string dir_path;
	//dir_path = "C:\\Users\\DELL\\Desktop\\代码-振动参数测量\\图像\\";
	dir_path = "/home/HwHiAiUser/pic/";
	Directory dir;
	vector<string> fileNames = dir.GetListFiles(dir_path,"*", false);
	string fileFullName;
#ifdef log
	printf("File : %s Line : %d\n\r", __FILE__, __LINE__);
#endif

	printf("fileNames.size() : %d\n\r", fileNames.size());

	totalImageCount = fileNames.size();
	for (int i = 0; i < fileNames.size(); i++)
	{
		fileFullName = dir_path + fileNames[i];
		cout << fileFullName << endl;
		EllipseDetection(fileFullName);
		//break;
	}
#ifdef log
	printf("File : %s Line : %d\n\r", __FILE__, __LINE__);	
#endif

	averageProcessTime = 1.0 * averageProcessTime / totalImageCount;
	averageProcessFrameRate = 1000.0 / averageProcessTime;
	
	printf("共处理图片 %d 张\n\r", totalImageCount);
	printf("平均每张处理时间：%.2lf ms\n\r", averageProcessTime);
	printf("平均处理帧率： %.2lf\n\r", averageProcessFrameRate);
	printf("end\n");
    return 0;
}
