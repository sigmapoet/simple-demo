#include <linux/module.h>
#define INCLUDE_VERMAGIC
#include <linux/build-salt.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

BUILD_SALT;

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__section(".gnu.linkonce.this_module") = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

MODULE_INFO(depends, "");

MODULE_ALIAS("pci:v00008088d00000000sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008088d00000101sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008088d00000102sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008088d00000103sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008088d00000104sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008088d00000105sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008088d00000106sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008088d00000107sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008088d00000108sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008088d00000100sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008088d00000109sv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008088d0000010Bsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008088d0000010Asv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00008088d0000010Csv*sd*bc*sc*i*");

MODULE_INFO(srcversion, "8FB4121AEEF94BB719EDA8F");
